def with_trace(function):
    def decorated(*args, **kwargs):
        args_unrolled = ', '.join([repr(arg) for arg in args]) if args is not () else None
        kwargs_unrolled = ', '.join(
            ['='.join([str(key_value[0]),
                       repr(key_value[1])])
            for key_value in kwargs.items()]) \
            if kwargs != {} else None
        try:
            parameters = ", ".join([args_unrolled, kwargs_unrolled])
        except TypeError:
            try:
                parameters = ", ".join([args_unrolled])
            except TypeError:
                parameters = ", ".join([kwargs_unrolled])

        name = function.__name__
        print(f'Calling {name}({parameters}) ...')
        result = function(*args, **kwargs)
        print('Done.')
        return result
    return decorated


@with_trace
def _spam(a, b, c):
    print(a, b, c)


if __name__ == '__main__':
    _spam(1, None, '')
    _spam(1, None, c='')
    _spam(1, b=None, c='')
    _spam(a=1, b=None, c='')
