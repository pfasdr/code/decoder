import numpy
import tensorflow
import random
import os
from tensorflow import keras


def seed_everything():
    """
    Set the seed values for every library used in this code base
    """
    seed_python()
    seed_numpy()
    seed_tensorflow_and_keras()


def seed_python():
    """
    The below is necessary in Python 3.2.3 onwards to have reproducible
    behavior for certain hash-based operations.

    See these references for further details:

    *   https://docs.python.org/3.4/using/cmdline.html#envvar-PYTHONHASHSEED
    *   https://github.com/keras-team/keras/issues/2280#issuecomment-306959926
    """
    os.environ['PYTHONHASHSEED'] = '42'

    # The below is necessary for starting core Python generated random numbers
    # in a well-defined state.

    random.seed(12345)


def seed_numpy():
    """
    The below is necessary for starting Numpy generated random numbers
    in a well-defined initial state.
    """
    numpy.random.seed(42)


def seed_tensorflow():
    """
    The below tf.set_random_seed() will make random number generation
    in the TensorFlow backend have a well-defined initial state.

    For further details, see:

    *   https://www.tensorflow.org/api_docs/python/tf/set_random_seed
    """
    tensorflow.set_random_seed(1234)


def seed_tensorflow_and_keras():
    """
    Force TensorFlow to use single thread.
    Multiple threads are a potential source of non-reproducible results.

    For further details, see:

    *   https://stackoverflow.com/questions/42022950/which-seeds-have-to-be-set-where-to-realize-100-reproducibility-of-training-res
    """
    seed_tensorflow()

    session_conf = tensorflow.ConfigProto(intra_op_parallelism_threads=1, inter_op_parallelism_threads=1)

    session = tensorflow.Session(graph=tensorflow.get_default_graph(),
                                 config=session_conf)
    keras.backend.set_session(session)
