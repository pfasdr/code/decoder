import contextlib

import sys


class _WriteOnlyFile(object):

    def write(self, x):
        pass


@contextlib.contextmanager
def no_std_out():
    save_stdout = sys.stdout
    sys.stdout = _WriteOnlyFile()
    yield
    sys.stdout = save_stdout
