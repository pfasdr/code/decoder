import csv


def ensure_csv_file(file_path, power_count):
    """
    Make sure a header of the given power count exists at the given path.
    """
    try:
        open(file_path)
        # TODO check internal structure
    except IOError:
        with open(file=file_path, mode='a') as csv_file:
            fieldnames = ['power%s' % index for index in range(power_count)]
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()
