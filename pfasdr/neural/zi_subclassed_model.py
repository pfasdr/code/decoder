import os

import tensorflow as tf
from tensorflow.python.distribute.mirrored_strategy import MirroredStrategy
from tensorflow.python.distribute.one_device_strategy import OneDeviceStrategy

from pfasdr.neural.zi_subclassed_model.main_module import learn_grid_constraints


def main():
    for node_count in range(5, 15 + 1):
        main_node_count(node_count)


def main_node_count(node_count):
    devices = [
        device.name[len('/physical_device:'):]
        for device in tf.config.list_physical_devices('GPU')
    ]

    strategy = get_strategy(devices)
    # strategy = OneDeviceStrategy(
    #     device='GPU:1'
    # )
    with strategy.scope():
        learn_grid_constraints(
            node_count=node_count,
            strategy=strategy,
            gpu_count=len(tf.config.list_physical_devices('GPU'))
        )


def get_strategy(devices):
    # num_packs = 128

    strategy = MirroredStrategy(
        devices=devices,
        # cross_device_ops=tf.distribute.CrossDeviceOps(),  # Fails
        cross_device_ops=tf.distribute.NcclAllReduce(
            # num_packs=num_packs,
        )
    )

    return strategy


if __name__ == '__main__':
    print(f'Available GPUs are: {tf.config.list_physical_devices("GPU")}')
    if not tf.test.is_built_with_gpu_support():
        raise NotImplementedError()
    if not tf.test.is_built_with_rocm():
        raise NotImplementedError()
    if not tf.config.list_physical_devices('GPU'):
        raise NotImplementedError()

    os.environ['AUTOGRAPH_VERBOSITY'] = '10'
    os.environ['NCCL_DEBUG'] = 'WARN'
    tf.get_logger().setLevel(
        # 'DEBUG'
        # 'INFO'
        'WARN'
    )
    tf.debugging.set_log_device_placement(
        # True
        False
    )

    main()
