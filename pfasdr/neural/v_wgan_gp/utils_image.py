import numpy as np
from numpy.ma.core import squeeze
from scipy.misc.pilutil import imsave, imresize


def save_images(*,
                images, size, path):
    return save_image(images=inverse_transform(images), size=size, path=path)


def save_image(*,
               images, size, path):
    image = squeeze(merge(images=images, size=size))

    imsave(path, image)


def merge_images(images):
    return inverse_transform(images)


def merge(*,
          images, size):
    h, w = images.shape[1], images.shape[2]
    if images.shape[3] in (3, 4):
        c = images.shape[3]
        img = np.zeros((h * size[0], w * size[1], c))
        for idx, image in enumerate(images):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w, :] = image
        return img
    elif images.shape[3] == 1:
        img = np.zeros((h * size[0], w * size[1]))
        for idx, image in enumerate(images):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w] = image[:,:,0]
        return img
    else:
        raise ValueError('in merge(images,size) images parameter ''must have dimensions: HxW or HxWx3 or HxWx4')


def center_crop(*,
                x, crop_h, crop_w, resize_h=64, resize_w=64):

    if crop_w is None:
        crop_w = crop_h

    h, w = x.shape[:2]

    j = int(round((h - crop_h)/2.))
    i = int(round((w - crop_w)/2.))

    return imresize(x[j:j+crop_h, i:i+crop_w], [resize_h, resize_w])


def transform(*,
              image, input_height, input_width, resize_height, resize_width, crop):
    if crop:
        cropped_image = center_crop(x=image, crop_h=input_height, crop_w=input_width, resize_h=resize_height, resize_w=resize_width)
    else:
        cropped_image = imresize(image, [resize_height, resize_width])
    return np.array(cropped_image)/127.5 - 1.


def inverse_transform(images):
    return (images + 1.) / 2.
