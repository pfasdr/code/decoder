import os


def ensure_path(directory_path):
    try:
        os.makedirs(directory_path)
    except OSError:
        pass

    return directory_path