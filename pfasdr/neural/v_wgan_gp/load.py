import gzip
import os

import numpy as np


def load_mnist(dataset_name):
    data_dir = os.path.join('..', '..', "./data", dataset_name)

    def extract_data(filename, num_data, head_size, data_size):
        with gzip.open(filename) as bytestream:
            bytestream.read(head_size)
            buffer = bytestream.read(data_size * num_data)
            data = np.frombuffer(buffer=buffer, dtype=np.uint8).astype(np.float)
        return data

    train_x_data = extract_data(data_dir + '/train-images-idx3-ubyte.gz', 60000, 16, 28 * 28)
    train_x = train_x_data.reshape((60000, 28, 28, 1))
    del train_x_data

    train_y_data = extract_data(data_dir + '/train-labels-idx1-ubyte.gz', 60000, 8, 1)
    train_y = train_y_data.reshape((60000, ))
    del train_y_data

    test_x_data = extract_data(data_dir + '/t10k-images-idx3-ubyte.gz', 10000, 16, 28 * 28)
    test_x = test_x_data.reshape((10000, 28, 28, 1))
    del test_x_data

    test_y_data = extract_data(data_dir + '/t10k-labels-idx1-ubyte.gz', 10000, 8, 1)
    test_y = test_y_data.reshape((10000, ))
    del test_y_data

    train_y = np.asarray(train_y)
    test_y = np.asarray(test_y)

    x = np.concatenate((train_x, test_x), axis=0)
    y = np.concatenate((train_y, test_y), axis=0).astype(np.int)

    # Shuffle both data sets the same way
    seed = 547
    np.random.seed(seed)
    np.random.shuffle(x)
    np.random.seed(seed)
    np.random.shuffle(y)

    y_vec = np.zeros((len(y), 10), dtype=np.float)
    for i, label in enumerate(y):
        y_vec[i, y[i]] = 1.0

    return x / 255., y_vec
