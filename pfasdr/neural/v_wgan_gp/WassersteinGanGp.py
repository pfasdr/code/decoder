import os
import time

import numpy as np
import tensorflow as tf

from pfasdr.neural.v_wgan_gp.utils_path import ensure_path
from pfasdr.neural.v_wgan_gp import load_mnist
from pfasdr.neural.v_wgan_gp.ops import lrelu, conv2d, batch_normalize, linear, deconv2d
from pfasdr.neural.v_wgan_gp import save_images


class WassersteinGanGp(object):
    model_name = "WGAN_GP"     # name for checkpoint

    def __init__(self, session, epoch, batch_size, z_dim, dataset_name, checkpoint_dir, result_dir, log_dir):
        if dataset_name not in ('mnist', 'fashion-mnist'):
            raise NotImplementedError

        self._session = session
        self._dataset_name = dataset_name
        self._checkpoint_dir = checkpoint_dir
        self._result_dir = result_dir
        self._log_dir = log_dir
        self._epoch = epoch
        self._batch_size = batch_size
        self._z_dim = z_dim         # dimension of noise-vector

        # parameters
        self._input_height = 28
        self._input_width = 28
        self._output_height = 28
        self._output_width = 28
        self._c_dim = 1

        # WGAN_GP parameter
        self._lambda = 0.25       # The higher value, the more stable, but the slower convergence
        self._d_iterations = 1     # The number of critic iterations for one-step of generator

        # train
        self._learning_rate = 0.0002
        self._beta_1 = 0.5

        # test
        self._evaluation_sample_count = 64  # number of generated images to be saved

        # load mnist
        self._data_x, self._data_y = load_mnist(self._dataset_name)

        # get number of batches for a single epoch
        self._num_batches = len(self._data_x) // self._batch_size

    def discriminator(self, *,
                      x, is_training, reuse):
        # Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
        # Architecture : (64)4c2s-(128)4c2s_BL-FC1024_BL-FC1_S
        with tf.variable_scope("discriminator", reuse=reuse):
            net = lrelu(conv2d(x, 64, 4, 4, 2, 2, name='d_conv1'))
            net = lrelu(batch_normalize(conv2d(net, 128, 4, 4, 2, 2, name='d_conv2'),
                                        is_training=is_training, scope='d_bn2'))
            net = tf.reshape(net, [self._batch_size, -1])
            net = lrelu(batch_normalize(linear(net, 1024, scope='d_fc3'),
                                        is_training=is_training, scope='d_bn3'))
            out_logit = linear(net, 1, scope='d_fc4')
            out = tf.nn.sigmoid(out_logit)

            return out, out_logit

    def generator(self, *,
                  z, is_training, reuse):
        # Network Architecture is exactly same as in infoGAN (https://arxiv.org/abs/1606.03657)
        # Architecture : FC1024_BR-FC7x7x128_BR-(64)4dc2s_BR-(1)4dc2s_S
        with tf.variable_scope("generator", reuse=reuse):
            net = tf.nn.relu(batch_normalize(linear(z, 1024, scope='g_fc1'),
                                             is_training=is_training, scope='g_bn1'))
            net = tf.nn.relu(batch_normalize(linear(net, 128 * 7 * 7, scope='g_fc2'),
                                             is_training=is_training, scope='g_bn2'))
            net = tf.reshape(net, [self._batch_size, 7, 7, 128])
            net = tf.nn.relu(
                batch_normalize(deconv2d(net, [self._batch_size, 14, 14, 64], 4, 4, 2, 2, name='g_dc3'),
                                is_training=is_training, scope='g_bn3'))

            out = tf.nn.sigmoid(deconv2d(net, [self._batch_size, 28, 28, 1], 4, 4, 2, 2, name='g_dc4'))

            return out

    def build_model(self):
        # some parameters
        image_dims = [self._input_height, self._input_width, self._c_dim]

        """ Graph Input """
        # images
        self.inputs = tf.placeholder(tf.float32, [self._batch_size] + image_dims, name='real_images')

        # noises
        self.z = tf.placeholder(tf.float32, [self._batch_size, self._z_dim], name='z')

        """ Loss Function """

        # output of d for real images
        d_real, d_real_logits = self.discriminator(x=self.inputs, is_training=True, reuse=False)

        # output of d for fake images
        g = self.generator(z=self.z, is_training=True, reuse=False)
        d_fake, d_fake_logits = self.discriminator(x=g, is_training=True, reuse=True)

        # get loss for discriminator
        d_loss_real = - tf.reduce_mean(d_real_logits)
        d_loss_fake = tf.reduce_mean(d_fake_logits)

        self.d_loss = d_loss_real + d_loss_fake

        # get loss for generator
        self.g_loss = - d_loss_fake

        """ Gradient Penalty """
        # This is borrowed from https://github.com/kodalinaveen3/DRAGAN/blob/master/DRAGAN.ipynb
        alpha = tf.random_uniform(shape=self.inputs.get_shape(), minval=0.,maxval=1.)
        differences = g - self.inputs # This is different from MAGAN
        interpolates = self.inputs + (alpha * differences)
        _, d_inter = self.discriminator(x=interpolates, is_training=True, reuse=True)
        gradients = tf.gradients(d_inter, [interpolates])[0]
        slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
        gradient_penalty = tf.reduce_mean((slopes - 1.) ** 2)
        self.d_loss += self._lambda * gradient_penalty

        """ Training """
        # divide trainable variables into a group for d and a group for g
        t_vars = tf.trainable_variables()
        d_vars = [var for var in t_vars if 'd_' in var.name]
        g_vars = [var for var in t_vars if 'g_' in var.name]

        # optimizers
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            #self.d_optimizer = tf.train.AdamOptimizer(self._learning_rate, beta1=self._beta_1) \
            #          .minimize(self.d_loss, var_list=d_vars)
            #self.g_optimizer = tf.train.AdamOptimizer(self._learning_rate * 5, beta1=self._beta_1) \
            #          .minimize(self.g_loss, var_list=g_vars)
            self.d_optimizer = tf.train.GradientDescentOptimizer(self._learning_rate) \
                      .minimize(self.d_loss, var_list=d_vars)
            self.g_optimizer = tf.train.GradientDescentOptimizer(self._learning_rate * 5) \
                      .minimize(self.g_loss, var_list=g_vars)

        """" Testing """
        # for test
        self.fake_images = self.generator(z=self.z, is_training=False, reuse=True)

        """ Summary """
        d_loss_real_summary = tf.summary.scalar("d_loss_real", d_loss_real)
        d_loss_fake_summary = tf.summary.scalar("d_loss_fake", d_loss_fake)
        d_loss_summary = tf.summary.scalar("d_loss", self.d_loss)
        g_loss_summary = tf.summary.scalar("g_loss", self.g_loss)

        # final summary operations
        self.g_summary = tf.summary.merge([d_loss_fake_summary, g_loss_summary])
        self.d_summary = tf.summary.merge([d_loss_real_summary, d_loss_summary])

    def train(self):

        # initialize all variables
        tf.global_variables_initializer().run()

        # graph inputs for visualize training results
        self.sample_z = np.random.uniform(-1, 1, size=(self._batch_size , self._z_dim))

        # saver to save model
        self.saver = tf.train.Saver()

        # summary writer
        self.writer = tf.summary.FileWriter(self._log_dir + '/' + self.model_name, self._session.graph)

        # restore check-point if it exits
        could_load, checkpoint_counter = self.load(self._checkpoint_dir)
        if could_load:
            start_epoch = (int)(checkpoint_counter / self._num_batches)
            start_batch_id = checkpoint_counter - start_epoch * self._num_batches
            counter = checkpoint_counter
            print(" [*] Load SUCCESS")
        else:
            start_epoch = 0
            start_batch_id = 0
            counter = 1
            print(" [!] Load failed...")

        # loop for epoch
        start_time = time.time()
        for epoch in range(start_epoch, self._epoch):

            # get batch data
            for idx in range(start_batch_id, self._num_batches):
                batch_images = self._data_x[idx * self._batch_size:(idx + 1) * self._batch_size]
                batch_z = np.random.uniform(-1, 1, [self._batch_size, self._z_dim]).astype(np.float32)

                # update d network
                _, summary_str, d_loss = self._session.run([self.d_optimizer, self.d_summary, self.d_loss],
                                                           feed_dict={self.inputs: batch_images, self.z: batch_z})
                self.writer.add_summary(summary_str, counter)

                # update G network
                if (counter-1) % self._d_iterations == 0:
                    batch_z = np.random.uniform(-1, 1, [self._batch_size, self._z_dim]).astype(np.float32)
                    _, summary_str, g_loss = self._session.run(
                        [self.g_optimizer, self.g_summary, self.g_loss],
                        feed_dict={self.z: batch_z}
                    )
                    self.writer.add_summary(summary_str, counter)

                counter += 1

                # display training status
                print("Epoch: [%2d] [%4d/%4d] time: %4.4f, d_loss: %.8f, g_loss: %.8f" \
                      % (epoch, idx, self._num_batches, time.time() - start_time, d_loss, g_loss))

                # save training results for every 300 steps
                if np.mod(counter, 300) == 0:
                    samples = self._session.run(self.fake_images,
                                                feed_dict={self.z: self.sample_z})
                    tot_num_samples = min(self._evaluation_sample_count, self._batch_size)
                    manifold_h = int(np.floor(np.sqrt(tot_num_samples)))
                    manifold_w = int(np.floor(np.sqrt(tot_num_samples)))
                    save_images(
                        images=samples[:manifold_h * manifold_w, :, :, :],
                        size=[manifold_h, manifold_w],
                        path='./' + ensure_path(self._result_dir + '/' + self.model_dir) + '/' + self.model_name + '_train_{:02d}_{:04d}.png'.format(epoch, idx)
                    )

            # After an epoch, start_batch_id is set to zero
            # non-zero value is only for the first epoch after loading pre-trained model
            start_batch_id = 0

            # save model
            self.save(self._checkpoint_dir, counter)

            # show temporal results
            self.visualize_results(epoch)

        # save model for final step
        self.save(self._checkpoint_dir, counter)

    def visualize_results(self, epoch):
        tot_num_samples = min(self._evaluation_sample_count, self._batch_size)
        image_frame_dim = int(np.floor(np.sqrt(tot_num_samples)))

        """ random condition, random noise """

        z_sample = np.random.uniform(-1, 1, size=(self._batch_size, self._z_dim))

        samples = self._session.run(self.fake_images, feed_dict={self.z: z_sample})

        save_images(
            images=samples[:image_frame_dim * image_frame_dim, :, :, :],
            size=[image_frame_dim, image_frame_dim],
            path=ensure_path(self._result_dir + '/' + self.model_dir) + '/' + self.model_name + '_epoch%03d' % epoch + '_test_all_classes.png'
        )

    @property
    def model_dir(self):
        model_dir = '_'.join([
            self.model_name, self._dataset_name, str(self._batch_size), str(self._z_dim)
        ])

        return model_dir

    def save(self, checkpoint_dir, step):
        checkpoint_dir = os.path.join(checkpoint_dir, self.model_dir, self.model_name)

        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        self.saver.save(self._session, os.path.join(checkpoint_dir, self.model_name + '.model'), global_step=step)

    def load(self, checkpoint_dir):
        import re
        print(" [*] Reading checkpoints...")
        checkpoint_dir = os.path.join(checkpoint_dir, self.model_dir, self.model_name)

        checkpoint = tf.train.get_checkpoint_state(checkpoint_dir)
        if checkpoint and checkpoint.model_checkpoint_path:
            checkpoint_name = os.path.basename(checkpoint.model_checkpoint_path)
            self.saver.restore(self._session, os.path.join(checkpoint_dir, checkpoint_name))
            counter = int(next(re.finditer("(\d+)(?!.*\d)", checkpoint_name)).group(0))
            print(" [*] Success to read {}".format(checkpoint_name))
            return True, counter
        else:
            print(" [*] Failed to find a checkpoint")
            return False, 0
