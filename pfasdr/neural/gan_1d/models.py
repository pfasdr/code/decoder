from pfasdr.util.paths import LAST_CHECKPOINT_PATH

from tensorflow.python.keras.engine.training import Model
from tensorflow.python.keras.optimizers import Adam, SGD


def get_models(get_generator_tensors, get_discriminator_tensors):
    sgd = SGD()

    discriminator_model = \
        _get_compiled_discriminator_model(
            get_discriminator_tensors=get_discriminator_tensors,
            optimizer=sgd)
    # Only train the generator in the combined training step
    discriminator_model.trainable = False

    # common optimizer
    # as described in appendix A of DeepMind's AC-GAN paper
    adam = Adam(lr=0.001,
                beta_1=0.9,
                beta_2=0.999)

    generator_input_tensor, generator_model =\
        _get_compiled_generator_model(
            get_generator_tensors=get_generator_tensors,
            optimizer=adam)

    combined_model = _get_compiled_combined_model(optimizer=adam,
                                                  discriminator_model=discriminator_model,
                                                  generator_input_tensor=generator_input_tensor,
                                                  generator_model=generator_model)

    return combined_model, discriminator_model, generator_model


def _get_compiled_combined_model(optimizer, discriminator_model, generator_input_tensor, generator_model):
    combined_output_tensor = discriminator_model(generator_model(generator_input_tensor))
    combined_model = Model(inputs=[generator_input_tensor],
                           outputs=[combined_output_tensor],
                               name='combined')

    combined_model.compile(optimizer=optimizer, loss='binary_crossentropy')
    return combined_model


def _get_compiled_discriminator_model(optimizer, get_discriminator_tensors):
    discriminator_model = get_discriminator_model(get_discriminator_tensors)

    discriminator_model.compile(optimizer=optimizer, loss='binary_crossentropy')

    try:
        discriminator_model.load_weights(LAST_CHECKPOINT_PATH, by_name=True)
    except OSError:
        pass

    return discriminator_model


def get_discriminator_model(get_discriminator_tensors):
    discriminator_input_tensor, discriminator_output_tensor = get_discriminator_tensors()
    discriminator_model = Model(inputs=[discriminator_input_tensor],
                                outputs=[discriminator_output_tensor],
                                name='discriminator')
    return discriminator_model


def _get_compiled_generator_model(optimizer, get_generator_tensors):
    generator_input_tensor, generator_model = get_generator_model(get_generator_tensors)

    generator_model.compile(optimizer=optimizer, loss='binary_crossentropy')

    try:
        generator_model.load_weights(LAST_CHECKPOINT_PATH, by_name=True)
    except OSError:
        pass

    return generator_input_tensor, generator_model


def get_generator_model(get_generator_tensors):
    generator_input_tensor, generator_output_tensor = get_generator_tensors()
    generator_model = Model(input=[generator_input_tensor],
                            output=[generator_output_tensor],
                            name='generator')
    return generator_input_tensor, generator_model
