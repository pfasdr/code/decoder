import os
import pickle

from PIL import Image

from pfasdr.neural.w_gan_1d import FIXED_NOISE
from pfasdr.neural.w_gan_1d import LAST_CHECKPOINT_PATH, MODEL_CHECKPOINT_PATH_TEMPLATE


def save_losses(combined_loss, disc_loss_generated, disc_loss_real):
    print('Saving losses ...')
    with open(os.path.join(LAST_CHECKPOINT_PATH, 'losses.pickle'), 'wb') as handle:
        pickle.dump({'combined_loss': combined_loss,
                     'disc_loss_real': disc_loss_real,
                     'disc_loss_generated': disc_loss_generated},
                    handle, protocol=pickle.HIGHEST_PROTOCOL)


def save_models(discriminator_model, generator_model, step_index):
    print('Saving model checkpoints ...')
    generator_model.save_weights(MODEL_CHECKPOINT_PATH_TEMPLATE.format('generator', step_index))
    discriminator_model.save_weights(MODEL_CHECKPOINT_PATH_TEMPLATE.format('discriminator', step_index))


def save_sample(generator_model, step_index):
    # plot batch of generated images with current generator
    z_generated = generator_model.predict(FIXED_NOISE)
    Image.fromarray(z_generated[0], mode='RGB').save(SAMPLE_CHECKPOINT_PATH_TEMPLATE.format(step_index))

    # save a batch of generated and real images to disk
    # x_training = BatchGenerator.get_batch()
    # figure_name = 'generated_image_batch_step_{}.png'.format(step_index)
    # print('Saving batch of generated images at adversarial step: {}.'.format(step_index))
    # plot_batch(numpy.concatenate((z_generated, x_training)), os.path.join(cache_dir, figure_name),
    #           label_batch=['generated'] * BATCH_SIZE + ['real'] * BATCH_SIZE)
