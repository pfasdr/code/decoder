"""
Standard GAN implemented on top of keras.
"""

import numpy

from pfasdr.neural.w_gan_1d import STEP_LIMIT, UPDATES_PER_STEP_DISCRIMINATOR, BATCH_SIZE, UPDATES_PER_STEP_GENERATOR, \
    LOG_INTERVAL, MODEL_SAVE_INTERVAL, LATENT_LENGTH

from pfasdr.neural.w_gan_1d import PlotBatchGenerator
from pfasdr.neural.w_gan_1d import get_models
from pfasdr.neural.w_gan_1d import save_sample, save_models, save_losses


def adversarial_training(get_discriminator_tensors, get_generator_tensors) -> None:
    """
    Adversarial training of the generator network Gθ and discriminator network Dφ.
    """
    combined_model, discriminator_model, generator_model =\
        get_models(get_discriminator_tensors=get_discriminator_tensors,
                   get_generator_tensors=get_generator_tensors)

    print('Generator:')
    generator_model.summary()
    print('Discriminator:')
    discriminator_model.summary()
    print('Combined:')
    combined_model.summary()
    
    losses_discriminator_real = numpy.empty(shape=1)
    losses_discriminator_generated = numpy.empty(shape=1)
    losses_generator = numpy.empty(shape=1)

    print('Adversarial Training ... ')
    for step_index in range(STEP_LIMIT):

        # train the discriminator
        for _ in range(UPDATES_PER_STEP_DISCRIMINATOR):
            discriminator_model.trainable = True

            # sample a mini-batch of real samples
            x_real = PlotBatchGenerator.get_batch()

            # update φ by taking an SGD step on mini-batch loss LD(φ)
            # TODO which labels to use?
            # y_training = numpy.random.uniform(low=0, high=1.0, size=BATCH_SIZE)
            y_real = numpy.ones(shape=(BATCH_SIZE, 1))
            discriminator_loss_real_now = discriminator_model.train_on_batch(x=x_real, y=y_real)
            losses_discriminator_real = numpy.append(arr=losses_discriminator_real, values=discriminator_loss_real_now)

            # sample a mini-batch of noise (generator input)
            z_random = numpy.random.normal(loc=0.5, scale=1.0, size=(BATCH_SIZE, LATENT_LENGTH))
            # generate a batch of samples with the current generator
            z_generated = generator_model.predict(x=z_random)

            # TODO which labels to use?
            # y_generated = numpy.random.uniform(low=0.0, high=0.3, size=BATCH_SIZE)
            y_generated = numpy.zeros(shape=(BATCH_SIZE, 1))
            discriminator_loss_now = discriminator_model.train_on_batch(x=z_generated, y=y_generated)
            losses_discriminator_generated = numpy.append(arr=losses_discriminator_generated, values=discriminator_loss_now)

        # train the generator
        for _ in range(UPDATES_PER_STEP_GENERATOR):
            discriminator_model.trainable = False

            z_random = numpy.random.normal(loc=0.5, scale=1.0, size=(BATCH_SIZE, LATENT_LENGTH))

            # update θ by taking an SGD step on mini-batch loss LR(θ)
            # y_training = numpy.random.normal(loc=0.5, scale=0.5, size=BATCH_SIZE)
            y_real = numpy.ones(shape=(BATCH_SIZE, 1))
            generator_loss_now = combined_model.train_on_batch(x=z_random, y=y_real)
            losses_generator = numpy.append(losses_generator, generator_loss_now)

        print('\rStep: {} of {}.'.format(step_index, STEP_LIMIT), end='')
        if step_index != 0 and not step_index % LOG_INTERVAL:
            print()

            print(
                f'Generator loss: {numpy.mean(losses_generator[-LOG_INTERVAL:], axis=0)}, '
                f'Discriminator loss real: {numpy.mean(losses_discriminator_real[-LOG_INTERVAL:], axis=0)}, '
                f'Discriminator loss generated: {numpy.mean(losses_discriminator_generated[-LOG_INTERVAL:], axis=0)} '
            )

        if step_index != 0 and not step_index % MODEL_SAVE_INTERVAL:
            print()

            print('Saving generated sample ...')
            #PoolManager.get_pool().apply_async(save_image, (generator_model, get_batch, step_index,))
            save_sample(generator_model, step_index)

            print('Saving model checkpoints ...')
            #PoolManager.get_pool().apply_async(save_models, (discriminator_model, generator_model, step_index,))
            save_models(discriminator_model, generator_model, step_index)

            print('Saving losses ...')
            #PoolManager.get_pool().apply_async(save_losses, (combined_loss, disc_loss_generated, disc_loss_real,))
            save_losses(losses_generator, losses_discriminator_generated, losses_discriminator_real)
