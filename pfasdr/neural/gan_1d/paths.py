import os

from pfasdr.util.paths import LAST_CHECKPOINT_PATH
from share.python_os.ensure_path import ensure_path

path = os.path.dirname(os.path.abspath(__file__))
LAST_CHECKPOINT_PATH = LAST_CHECKPOINT_PATH
ensure_path(LAST_CHECKPOINT_PATH)

MODEL_CHECKPOINT_PATH_TEMPLATE = os.path.join(LAST_CHECKPOINT_PATH, '{}_model_weights_step_{}.h5')

IMAGE_CHECKPOINT_PATH_TEMPLATE = os.path.join(LAST_CHECKPOINT_PATH, 'generated_image_step_{}.png')
