import numpy

from pfasdr.grid_gridcal.sampler.c_plot_samples_util import get_valid_powers_from_situations
from pfasdr.neural.w_gan_1d import BATCH_SIZE, PROBLEM_WIDTH, CHANNEL_COUNT


def _powers_valid_gen():
    while True:
        situations_valid = get_valid_powers_from_situations(limit=100000)
        for situation_index in range(situations_valid.shape[0]):
            situation = situations_valid.iloc[situation_index]
            yield situation


class PlotBatchGenerator(object):

    situations = None

    @classmethod
    def get_batch(cls):
        if cls.situations is None:
            cls.situations = _powers_valid_gen()

        batch = numpy.empty((BATCH_SIZE, PROBLEM_WIDTH, CHANNEL_COUNT))

        for situation_index, situation in enumerate(cls.situations):
            if situation_index == BATCH_SIZE:
                break
            for power_index, power in enumerate(situation):
                power /= 20  # normalize
                batch[situation_index][power_index][0] = power

        return batch


if __name__ == '__main__':
    # TODO This only produces about 40 batches/s.
    # TODO So make it load batches faster!

    batches_seen = set()
    batches_requested = 0
    while True:
        batch_digest = numpy.mean(PlotBatchGenerator.get_batch())
        batches_seen.add(batch_digest)

        batches_requested += 1

        if batches_requested % 40 == 0:
            print(batches_requested, len(batches_seen))
