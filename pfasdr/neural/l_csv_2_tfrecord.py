from os import cpu_count, linesep, path
from csv import DictReader, DictWriter
from glob import glob
from multiprocessing.pool import Pool

import tensorflow as tf

from pfasdr.util.paths import LAST_SITUATIONS_PATH
from pfasdr.util.situations_persistence import FIELDNAMES


def _int64_feature(int64_value):
    int64_list = tf.train.Int64List(value=[int64_value])
    int64_feature = tf.train.Feature(int64_list=int64_list)

    return int64_feature


def _float64_feature(float64_value):
    """
    Create and return a Tensorflow bytes feature from 64-bit float value.

    The float value gets stored as a byte representation of a string.
    This avoids loss of precision when converting between float32 and float64.
    :param float64_value: A value of type float64 to store.
    :return: A tensorflow feature containing a bytes list.
    """
    float64_bytes = [str(float64_value).encode()]
    bytes_list = tf.train.BytesList(value=float64_bytes)
    bytes_list_feature = tf.train.Feature(bytes_list=bytes_list)

    return bytes_list_feature


def csvs2tfrecords():
    """
    Convert CSV files to tf record files.
    """
    csv_file_names = glob(path.join(LAST_SITUATIONS_PATH, '*.csv'))
    # Run the actual conversion in a process per file
    Pool(cpu_count()).map(write_tfrecord, csv_file_names)


def write_tfrecord(csv_file_path):
    print('Converting CSV files to Tensorflow record files.')

    print('Reading from:', csv_file_path)

    tfrecord_filename = csv_file_path[:-len('.csv')] + '.tfrecord'
    print('Writing to:', tfrecord_filename)

    with tf.python_io.TFRecordWriter(tfrecord_filename) as writer:

        with open(csv_file_path) as csv_file:
            reader = DictReader(
                f=csv_file,
                fieldnames=FIELDNAMES
            )
            next(reader)  # Skip headers
            for index, example in enumerate(reader):
                label = 1 if example['Validity'] == 'True' else 0
                del example['Validity']

                tap0, tap1 = int(example['Tap0']), int(example['Tap1'])
                del example['Tap0']
                del example['Tap1']

                powers = [example[key] for key in sorted(example.keys())]
                powers = [float(value) for value in powers]
                power0, power1, power2, power3, power4, power5, power6 = powers
                del powers

                features = {
                    'Tap0': _int64_feature(tap0),
                    'Tap1': _int64_feature(tap1),
                    'power0': _float64_feature(power0),
                    'power1': _float64_feature(power1),
                    'power2': _float64_feature(power2),
                    'power3': _float64_feature(power3),
                    'power4': _float64_feature(power4),
                    'power5': _float64_feature(power5),
                    'power6': _float64_feature(power6),
                    'Validity': _int64_feature(label),
                }
                del example

                example_features = tf.train.Features(feature=features)
                example = tf.train.Example(features=example_features)
                example_string = example.SerializeToString()
                writer.write(example_string)

                if not index % 100:
                    print('\r' + str(index), end='')
            print('\r', end='')


def deserialize(serialized_example):
    """
    Parses an image and label from the given `serialized_example`.
    """
    features = {
        'power0': tf.FixedLenFeature([], tf.string),
        'power1': tf.FixedLenFeature([], tf.string),
        'power2': tf.FixedLenFeature([], tf.string),
        'power3': tf.FixedLenFeature([], tf.string),
        'power4': tf.FixedLenFeature([], tf.string),
        'power5': tf.FixedLenFeature([], tf.string),
        'power6': tf.FixedLenFeature([], tf.string),
        'Tap0': tf.FixedLenFeature([], tf.int64),
        'Tap1': tf.FixedLenFeature([], tf.int64),
        'Validity': tf.FixedLenFeature([], tf.int64),
    }
    features = tf.parse_single_example(
        serialized_example,
        features=features
    )

    # cast from bytes to floats
    features['power0'] = tf.strings.to_number(string_tensor=features['power0'], out_type=tf.float64)
    features['power1'] = tf.strings.to_number(string_tensor=features['power1'], out_type=tf.float64)
    features['power2'] = tf.strings.to_number(string_tensor=features['power2'], out_type=tf.float64)
    features['power3'] = tf.strings.to_number(string_tensor=features['power3'], out_type=tf.float64)
    features['power4'] = tf.strings.to_number(string_tensor=features['power4'], out_type=tf.float64)
    features['power5'] = tf.strings.to_number(string_tensor=features['power5'], out_type=tf.float64)
    features['power6'] = tf.strings.to_number(string_tensor=features['power6'], out_type=tf.float64)

    return \
        features['power0'],\
        features['power1'],\
        features['power2'],\
        features['power3'],\
        features['power4'],\
        features['power5'],\
        features['power6'], \
        features['Tap0'], \
        features['Tap1'], \
        features['Validity']


def tfrecords2csvs(writer=None):
    """
    Convert CSV files to Tensorflow record files via the given converter.
    """
    csv_file_paths = glob(path.join(LAST_SITUATIONS_PATH, '*.csv'))

    # Run the actual conversion in a process per file
    Pool(cpu_count()).map(writer, csv_file_paths)


def write_csv_via_python_io(csv_file_path):
    """
    Convert the given CSV file to a Tensorflow record file via Python IO.
    """
    print('Converting Tensorflow record files to CSV files via Python IO.')

    tfrecord_file_path = csv_file_path[:-len('.csv')] + '.tfrecord'

    print('Reading from:', tfrecord_file_path)
    print('Writing to:', csv_file_path)

    with open(csv_file_path, 'w', newline='') as csv_file:
        csv_writer = DictWriter(
            f=csv_file,
            fieldnames=FIELDNAMES,
            restval='the dictionary is missing a key in fieldnames',
            extrasaction='raise',
            dialect='excel',
            lineterminator=linesep,
        )
        csv_writer.writeheader()

        record_iterator = tf.python_io.tf_record_iterator(path=tfrecord_file_path)
        for index, string_record in enumerate(record_iterator):
            example = tf.train.Example()
            example.ParseFromString(string_record)

            example_dict = dict()
            example_dict['Validity'] = bool(example.features.feature['Validity'].int64_list.value[0])
            example_dict['power0'] = float(example.features.feature['power0'].bytes_list.value[0].decode())
            example_dict['power1'] = float(example.features.feature['power1'].bytes_list.value[0].decode())
            example_dict['power2'] = float(example.features.feature['power2'].bytes_list.value[0].decode())
            example_dict['power3'] = float(example.features.feature['power3'].bytes_list.value[0].decode())
            example_dict['power4'] = float(example.features.feature['power4'].bytes_list.value[0].decode())
            example_dict['power5'] = float(example.features.feature['power5'].bytes_list.value[0].decode())
            example_dict['power6'] = float(example.features.feature['power6'].bytes_list.value[0].decode())
            example_dict['Tap0'] = example.features.feature['Tap0'].int64_list.value[0]
            example_dict['Tap1'] = example.features.feature['Tap1'].int64_list.value[0]

            csv_writer.writerow(example_dict)
            csv_file.flush()

            if not index % 100:
                print('\r' + str(index), end='')
        print('\r', end='')


def write_csv_via_dataset(csv_file_path):
    """
    Convert the given CSV file to a Tensorflow record file via data set.
    """
    print('Converting Tensorflow record files to CSV files via data set.')

    tfrecord_file_path = csv_file_path[:-len('.csv')] + '.tfrecord'

    print('Reading from:', tfrecord_file_path)
    print('Writing to:', csv_file_path)

    with open(csv_file_path, 'w', newline='') as csv_file:
        csv_writer = DictWriter(
            f=csv_file,
            fieldnames=FIELDNAMES,
            restval='the dictionary is missing a key in fieldnames',
            extrasaction='raise',
            dialect='excel',
            lineterminator=linesep,
        )
        csv_writer.writeheader()

        with tf.name_scope('input'):
            dataset = tf.data.TFRecordDataset(tfrecord_file_path)
            dataset = dataset.map(deserialize)
            iterator = dataset.make_one_shot_iterator()
            next_element = iterator.get_next()

            # the conversion does only scale on CPU
            config = tf.ConfigProto(
                device_count={'GPU': 0}
            )
            session = tf.Session(config=config)

            index = 0
            while True:
                try:
                    example = session.run(next_element)
                    index += 1

                    example_dict = dict()
                    example_dict['power0'] = example[0]
                    example_dict['power1'] = example[1]
                    example_dict['power2'] = example[2]
                    example_dict['power3'] = example[3]
                    example_dict['power4'] = example[4]
                    example_dict['power5'] = example[5]
                    example_dict['power6'] = example[6]
                    example_dict['Tap0'] = example[7]
                    example_dict['Tap1'] = example[8]
                    example_dict['Validity'] = example[9] == 1

                    csv_writer.writerow(example_dict)
                    csv_file.flush()

                    if not index % 100:
                        print('\r' + str(index), end='')
                except tf.errors.OutOfRangeError:
                    break
        print('\r', end='')


if __name__ == '__main__':
    csvs2tfrecords()
    tfrecords2csvs(writer=write_csv_via_python_io)
    tfrecords2csvs(writer=write_csv_via_dataset)  # Works too, but is way slower
