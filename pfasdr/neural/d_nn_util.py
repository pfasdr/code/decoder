import functools
import glob
import multiprocessing
import os

# import numpy as numpy
import pandas
# import tensorflow as tensorflow
# from numpy.core.multiarray import ndarray
from sklearn.model_selection._split import train_test_split
# from tensorflow import keras
# from tensorflow.python.estimator import keras
# from tensorflow.python.estimator import keras
from tensorflow import keras

from pfasdr.neural.SeqGAN.config import COMMON_EMBEDDINGS_COUNT
# from pfasdr.neural.gan_1d.config import CHANNEL_COUNT
from pfasdr.util.paths import LAST_SITUATIONS_PATH
from pfasdr.util.situations_persistence import FIELDNAMES, \
    SITUATIONS_DTYPE, COLUMN_RANGE_INPUT, COLUMN_RANGE_OUTPUT, HEADER_HEIGHT,\
    COLUMN_NAMES_INPUT, COLUMN_NAMES_LABELS


def load_data_set_from_ssv_files(file_path, limit):
    data = pandas.read_csv(
        # These from parameters:
        filepath_or_buffer=file_path,
        nrows=limit,
        # These by convention:
        delimiter=" ",
        names=FIELDNAMES,
        skiprows=0,
        dtype=SITUATIONS_DTYPE)

    max_power = 12  # per circuit sampling
    max_integer = 2 ** 63 - 1  # 64 bit integer
    factor = (COMMON_EMBEDDINGS_COUNT / max_power * max_integer) / max_integer
    # integers = [int(power * factor) for power in line]
    data = data / factor

    return data


def load_data_subset_from_csv_file(file_path, use_columns):
    """
    Load the requested data from the given file
    """
    data = pandas.read_csv(
        # These from parameters:
        filepath_or_buffer=file_path,
        # nrows=limit,
        usecols=use_columns,
        # These by convention:
        delimiter=",",
        names=FIELDNAMES,
        skiprows=HEADER_HEIGHT,
        dtype=SITUATIONS_DTYPE,
    )

    return data


class PoolManager(object):
    pool = None

    @classmethod
    def get_pool(self):
        if PoolManager.pool is None:
            PoolManager.pool = multiprocessing. \
                Pool(multiprocessing.cpu_count())
        return PoolManager.pool


def load_valid_data_set_from_csv_files(use_columns):
    """
    Load the requested data set from the given files
    """

    file_paths = glob.glob(pathname=os.path.join(LAST_SITUATIONS_PATH, '*_valid.csv'),
                           recursive=False)

    data_set = _get_data_set(file_paths, use_columns)

    return data_set


def load_invalid_data_set_from_csv_files(use_columns):
    """
    Load the requested data set from the given files
    """

    file_paths = glob.glob(pathname=os.path.join(LAST_SITUATIONS_PATH, '*_invalid.csv'),
                           recursive=False)

    data_set = _get_data_set(file_paths, use_columns)

    return data_set


def _get_data_set(file_paths, use_columns):
    function_partial = functools.partial(
        load_data_subset_from_csv_file,
        use_columns=use_columns)

    data_subsets = PoolManager.get_pool().map(function_partial, file_paths)

    data_set = pandas.concat(data_subsets)

    return data_set


def get_valid_data():
    """
    Load the valid data from the CSV files and return it's x and y.
    """
    x = load_valid_data_set_from_csv_files(
        use_columns=COLUMN_RANGE_INPUT)
    y = load_valid_data_set_from_csv_files(
        use_columns=COLUMN_RANGE_OUTPUT)

    return x, y


def get_invalid_data():
    """
    Load the valid data from the CSV files and return it's x and y.
    """
    x = load_invalid_data_set_from_csv_files(
        use_columns=COLUMN_RANGE_INPUT)
    y = load_invalid_data_set_from_csv_files(
        use_columns=COLUMN_RANGE_OUTPUT)

    return x, y


def load_valid_data_sets_from_csv_files_normalized():
    """
    Load the input and output data necessary to train and test a neural network from csv files
    """
    x, y = get_valid_data()

    x = x.values
    y = y.values  # For consistency's sake

    # print(ndarray.max(x, axis=1))

    # TODO Document this
    x = keras.utils.normalize(
        x,
        axis=1,
        order=2
    )

    # print(ndarray.max(x, axis=1))

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

    return x_train, y_train, x_test, y_test


def load_invalid_data_sets_from_csv_files_normalized():
    """
    Load the input and output data necessary to train and test a neural network from csv files
    """
    x, y = get_invalid_data()

    x = x.values
    y = y.values  # For consistency's sake

    # print(ndarray.max(x, axis=1))

    # TODO Document this
    x = keras.utils.normalize(
        x,
        axis=1,
        order=2
    )

    # print(ndarray.max(x, axis=1))

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)

    return x_train, y_train, x_test, y_test


def load_valid_data_sets_from_csv_files_data_frame():
    """
    Load the input and output data necessary to train and test a neural network from csv files
    """
    x, y = get_valid_data()

    if not isinstance(x, pandas.DataFrame):
        raise
    if not isinstance(y, pandas.DataFrame):
        raise

    # x_train = x.sample(frac=0.7, random_state=321)
    # x_test = x.drop(x_train.index)
    # y_train = y.sample(frac=0.7, random_state=321)
    # y_test = y.drop(y_train.index)

    import pandas as pd
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
    x_train = pd.DataFrame.from_records(x_train)
    x_test = pd.DataFrame.from_records(x_test)
    y_train = pd.DataFrame.from_records(y_train)
    y_test = pd.DataFrame.from_records(y_test)

    if not isinstance(x_train, pandas.DataFrame):
        print(type(x_train))
        raise
    if not isinstance(x_test, pandas.DataFrame):
        print(type(x_test))
        raise
    if not isinstance(y_train, pandas.DataFrame):
        print(type(y_train))
        raise
    if not isinstance(y_test, pandas.DataFrame):
        print(type(y_test))
        raise

    return x_train, y_train, x_test, y_test


def load_invalid_data_sets_from_csv_files_data_frame():
    """
    Load the input and output data necessary to train and test a neural network from csv files
    """
    x, y = get_invalid_data()

    if not isinstance(x, pandas.DataFrame):
        raise
    if not isinstance(y, pandas.DataFrame):
        raise

    #x_train = x.sample(frac=0.7, random_state=321)
    #x_test = x.drop(x_train.index)
    #y_train = y.sample(frac=0.7, random_state=321)
    #y_test = y.drop(y_train.index)

    import pandas as pd
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
    x_train = pd.DataFrame.from_records(x_train)
    x_test = pd.DataFrame.from_records(x_test)
    y_train = pd.DataFrame.from_records(y_train)
    y_test = pd.DataFrame.from_records(y_test)

    if not isinstance(x_train, pandas.DataFrame):
        print(type(x_train))
        raise
    if not isinstance(x_test, pandas.DataFrame):
        print(type(x_test))
        raise
    if not isinstance(y_train, pandas.DataFrame):
        print(type(y_train))
        raise
    if not isinstance(y_test, pandas.DataFrame):
        print(type(y_test))
        raise

    return x_train, y_train, x_test, y_test


def _main():
    print('Loading data ...')
    x_train, y_train, x_test, y_test = load_valid_data_sets_from_csv_files_normalized()

    for name, column_set, data_set in zip(
            ('x_train', 'y_train', 'x_test', 'y_test'),
            (COLUMN_NAMES_INPUT, COLUMN_NAMES_LABELS, COLUMN_NAMES_INPUT, COLUMN_NAMES_LABELS),
            (x_train, y_train, x_test, y_test)):

        print(name + ' data shape: ' + x_train.shape)
        example_value = data_set[column_set[0]][0]
        print('Example value:\n' + example_value)
        del example_value
        print()


if __name__ == '__main__':
    _main()
