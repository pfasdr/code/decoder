import numpy as np

from pfasdr.neural.ze_discriminate_pd_np.mask_devices import mask_devices
from pfasdr.util.paths import ROOT_PATH

np.set_printoptions(precision=3, suppress=True)

from pfasdr.neural.ze_discriminate_pd_np.learn_grid_constraints_module import \
    learn_grid_constraints


def main():
    # mask_devices(visible=None)

    # batch_size_training = 4194304
    # batch_size_training = 2097152
    # batch_size_training = 1048576
    # batch_size_training = 524288
    # batch_size_training = 262144
    # batch_size_training = 131072
    # batch_size_training = 65536
    # batch_size_training = 32768
    # batch_size_training = 16384
    # batch_size_training = 8192
    # batch_size_training = 4096
    # batch_size_training = 2048
    # batch_size_training = 1024
    # batch_size_training = 512
    batch_size_training = 256
    # batch_size_training = 128
    # batch_size_training = 64    # 2 GPU 2nd epoch: ___s/epoch _ms/step ___% CPU
    # batch_size_training = 32
    # batch_size_training = 16
    # batch_size_training = 8
    # batch_size_training = 4
    # batch_size_training = 2
    # batch_size_training = 1

    epochs_training = 50

    node_count = 7
    valid_file_path = \
        ROOT_PATH / 'data' / 'PFASDR.Data.15-node-dynsim-benchmark' / \
        'by sampling' / '2018-03-13' / 'situations' / 'valid.csv'
    invalid_file_path = \
        ROOT_PATH / 'data' / 'PFASDR.Data.15-node-dynsim-benchmark' / \
        'by sampling' / '2018-03-13' / 'situations' / 'valid.csv'

    learn_grid_constraints(
        node_count=node_count,
        batch_size=batch_size_training,
        epochs_training=epochs_training,
        invalid_file_path=invalid_file_path,
        valid_file_path=valid_file_path,
    )


if __name__ == '__main__':
    main()
