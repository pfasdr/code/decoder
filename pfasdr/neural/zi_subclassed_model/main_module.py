import tensorflow
from math import sqrt
from pathlib import Path
from statistics import mean

from tensorflow import Tensor
from tensorflow.python.distribute.mirrored_strategy import MirroredStrategy
from tensorflow.python.keras.losses import BinaryCrossentropy
from tensorflow.python.keras.metrics import Mean, BinaryAccuracy
from tensorflow.python.keras.optimizer_v2.adam import Adam

from pfasdr.neural.zi_subclassed_model.neural_grid_model_module import \
    NeuralGridModel
from pfasdr.neural.zi_subclassed_model.test_step_factory_module import \
    test_step_factory
from pfasdr.neural.zi_subclassed_model.train_step_factory_module import \
    train_step_factory
from pfasdr.neural.ze_discriminate_pd_np.get_datasets_module import get_datasets
from pfasdr.neural.ze_discriminate_pd_np.path_templates_module import \
    valid_file_path_template, invalid_file_path_template
from pfasdr.util.paths import grid_surrogates_path


def learn_grid_constraints(
    *,
    node_count,
    strategy,
    gpu_count,
):
    grid_surrogate_path: Path = \
        grid_surrogates_path / 'linear_grid' / f'length_{node_count}'

    model: NeuralGridModel = NeuralGridModel(node_count=node_count)

    loss_object = BinaryCrossentropy(
        from_logits=True,
        label_smoothing=0,
        # reduction=losses_utils.ReductionV2.AUTO,
        reduction=tensorflow.keras.losses.Reduction.NONE,  # mirrored strategy
        name='binary_crossentropy',
    )

    train_loss = Mean(name='Mean Training Loss')
    train_accuracy = BinaryAccuracy(name='Training Binary Accuracy')
    test_loss = Mean(name='Mean Testing Loss')
    test_accuracy = BinaryAccuracy(name='Testing Binary Accuracy')

    optimizer = Adam(
        # learning_rate=0.001,  # Default
        learning_rate=0.0002,  # More exploitation, less exploration
        beta_1=0.9,
        beta_2=0.999,
        epsilon=1e-07,
        amsgrad=False,
        name='Adam',
    )

    model.build(input_shape=(1, node_count))

    # batch_size = 100_000  # 90 % load on 1 Vega 64 learning.
    batch_size = 200_000 * gpu_count  # 90 % load on Mirrored Vegas

    train_step = train_step_factory(
        model=model,
        loss_object=loss_object,
        optimizer=optimizer,
        global_batch_size=batch_size,
        mirrored_strategy=strategy,
    )

    test_step = test_step_factory(
        model=model,
        test_loss=test_loss,
        loss_function=loss_object,
        test_accuracy=test_accuracy,
        mirrored=isinstance(strategy, MirroredStrategy),
    )

    valid_file_path = valid_file_path_template.substitute(
        length=node_count,
    )
    invalid_file_path = invalid_file_path_template.substitute(
        length=node_count,
    )

    dataset_training, dataset_validation, dataset_testing, \
        dataset_size_training, dataset_size_validation, dataset_size_testing \
        = get_datasets(
            batch_size=batch_size,
            names=list(range(node_count)),
            invalid_file_path=invalid_file_path,
            valid_file_path=valid_file_path,
            repeat_datasets=False,
        )

    dataset_training = \
        strategy.experimental_distribute_dataset(dataset_training)
    dataset_validation = \
        strategy.experimental_distribute_dataset(dataset_validation)
    dataset_testing = \
        strategy.experimental_distribute_dataset(dataset_testing)

    epochs = 2000
    steps_training = 100
    steps_testing = 1
    target_test_accuracy = 0.9
    target_test_loss = 0.55

    for epoch in range(epochs):
        # Reset the metrics at the start of the next epoch
        train_loss.reset_states()
        train_accuracy.reset_states()
        test_loss.reset_states()
        test_accuracy.reset_states()

        print('Training ...')
        for _ in range(steps_training):
            for features_training_and_labels_training in dataset_training:
                train_step(
                    features_training_and_labels_training
                )
                break

        for features_testing_and_labels_testing_per_replica in dataset_testing:
            features_testing_per_replica, labels_testing_per_replica = \
                features_testing_and_labels_testing_per_replica

            # Package Tensor objects as if they were PerReplica objects
            if isinstance(features_testing_per_replica, Tensor):
                features_testing_per_replica.values = \
                    (features_testing_per_replica, )
            if isinstance(labels_testing_per_replica, Tensor):
                labels_testing_per_replica.values = \
                    (labels_testing_per_replica, )

            for replica_index in \
                    range(len(labels_testing_per_replica.values)):
                labels_testing = \
                    labels_testing_per_replica.values[replica_index][:100]
                features_testing = \
                    features_testing_per_replica.values[replica_index][:100]

                labels_prediction = model.predict_step(
                    data=features_testing,
                )
                labels_testing = [
                    label[0] for label in labels_testing.numpy().tolist()
                ]
                labels_prediction = [
                    round(label[0], 3)
                    for label in labels_prediction.numpy().tolist()
                ]
                root_square_label_errors = [
                    sqrt((label_testing - label_prediction) ** 2) for
                    label_testing, label_prediction in
                    zip(labels_testing, labels_prediction)
                ]
                print(
                    f'Example for replica {replica_index} - '
                    'Prediction Labels:', str(labels_prediction[:5]) + ', ',
                    'Testing Labels:', str(labels_testing[:5]) + ', ',
                    'Errors:', str(root_square_label_errors[:5]) + ', ',
                    'Root Mean Squared Error:',
                    str(round(mean(root_square_label_errors), 8))
                    .ljust(10, '0'),
                )

            model.save(
                filepath=grid_surrogate_path,
                overwrite=True,
                include_optimizer=True,
                save_format='tf',
            )

            print('Testing ...')
            for _ in range(steps_testing):
                for features_testing_and_labels_testing_per_replica \
                        in dataset_testing:
                    test_step(
                        dist_inputs=
                        features_testing_and_labels_testing_per_replica,
                    )
                    break

            print(
                f'Epoch {epoch + 1} - ',
                f'{train_loss.name}: {train_loss.result()}, '
                f'{train_accuracy.name}: {train_accuracy.result() * 100} %, '
                f'{test_loss.name}: {test_loss.result()}, '
                f'{test_accuracy.name}: {test_accuracy.result() * 100} %'
            )

            break

        # If quality targets have been reached, stop early.
        if test_accuracy.result() > target_test_accuracy \
                and test_loss.result() < target_test_loss:
            break

    print('Validating ...')
    # Reset the metrics before validating
    test_loss.reset_states()
    test_accuracy.reset_states()

    steps_validation = 1
    for _ in range(steps_validation):
        for features_testing_and_labels_testing_per_replica \
                in dataset_validation:
            test_step(
                dist_inputs=
                features_testing_and_labels_testing_per_replica,
            )
            break

    print(
        'Validation result - '
        f'{test_loss.name}: {test_loss.result()}, '
        f'{test_accuracy.name}: {test_accuracy.result() * 100} %'
    )

    for features_testing_and_labels_testing_per_replica in dataset_testing:
        features_testing_per_replica, labels_testing_per_replica = \
            features_testing_and_labels_testing_per_replica

        # Package Tensor objects as if they were PerReplica objects
        if isinstance(features_testing_per_replica, Tensor):
            features_testing_per_replica.values = \
                (features_testing_per_replica,)
        if isinstance(labels_testing_per_replica, Tensor):
            labels_testing_per_replica.values = \
                (labels_testing_per_replica,)

        for replica_index in \
                range(len(labels_testing_per_replica.values)):
            labels_testing = \
                labels_testing_per_replica.values[replica_index][:100]
            features_testing = \
                features_testing_per_replica.values[replica_index][:100]

            labels_prediction = model.predict_step(
                data=features_testing,
            )
            print(
                'labels_prediction:', str(labels_prediction.numpy()),
                'labels_testing:', str(labels_testing.numpy()),
            )
        break

    print(f'Saving grid surrogate under: {grid_surrogate_path}')
    model.save(
        filepath=grid_surrogate_path,
        overwrite=True,
        include_optimizer=True,
        save_format='tf',
        signatures=None,
        options=None,
    )
