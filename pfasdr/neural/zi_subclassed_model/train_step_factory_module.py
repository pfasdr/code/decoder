import tensorflow
from tensorflow import function, GradientTape
from tensorflow.python.distribute.values import PerReplica


def train_step_factory(
    *,
    model,
    loss_object,
    optimizer,
    global_batch_size,
    mirrored_strategy,
):
    @function(autograph=True)
    def compute_loss(labels, predictions):
        per_example_loss = loss_object(labels, predictions)
        return tensorflow.nn.compute_average_loss(
            per_example_loss,
            global_batch_size=global_batch_size,
        )

    @function(autograph=True)
    def train_step(
        features_training_and_labels_training: PerReplica,
    ):
        print("Python execution: ", features_training_and_labels_training)
        features_training, labels_training = \
            features_training_and_labels_training

        with GradientTape() as tape:
            predictions = model.call_training(features_training)
            loss_value = compute_loss(
                labels=labels_training,
                predictions=predictions,
            )

        gradients = tape.gradient(loss_value, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        return loss_value

    @function(autograph=True)
    def distributed_train_step(dist_inputs):
        per_replica_losses = mirrored_strategy.run(
            train_step,
            args=(dist_inputs,)
        )

        return mirrored_strategy.reduce(
            tensorflow.distribute.ReduceOp.SUM,
            per_replica_losses,
            axis=None,
        )

    return distributed_train_step
