from os import cpu_count

import tensorflow as tf

from pfasdr.neural.l_csv_2_tfrecord import deserialize
from pfasdr.util.paths import LAST_TFRECORD_FILE_PATHS

_EPOCHS = 5
_NUM_CLASSES = 10
_BATCH_SIZE = 128


def training_pipeline():
    training_set = data_generator(is_training=True)
    testing_set = data_generator(is_training=False)

    model = keras_model()
    model.compile('adam', 'categorical_crossentropy', metrics=['acc'])
    steps_per_epoch = (10 ** 3 * 0.8) // _BATCH_SIZE
    validation_data = testing_set.make_one_shot_iterator()
    validation_steps = (10 ** 3 * 0.2) // _BATCH_SIZE
    model.fit(
        x=training_set.make_one_shot_iterator(),
        steps_per_epoch=steps_per_epoch,
        epochs=_EPOCHS,
        validation_data=validation_data,
        validation_steps=validation_steps,
        verbose=1
    )


def data_generator(is_training):
    """
    Construct a data generator using tf.Dataset
    """

    dataset = tf.data.TFRecordDataset(LAST_TFRECORD_FILE_PATHS)
    dataset = dataset.map(deserialize, num_parallel_calls=cpu_count())

    if is_training:
        dataset = dataset.shuffle(1000)  # depends on sample size

    # Transform and batch data at the same time
    #dataset = dataset.apply(tf.contrib.data.map_and_batch(
    #    deserialize, _BATCH_SIZE,
    #    num_parallel_batches=cpu_count(),
    #    drop_remainder=True if is_training else False))
    dataset = dataset.repeat()
    dataset = dataset.prefetch(tf.contrib.data.AUTOTUNE)

    return dataset


def keras_model():
    from tensorflow.python.keras.layers import Conv2D, MaxPool2D, Flatten, Dense, Dropout, Input

    inputs = Input(shape=(28, 28, 1))
    x = Conv2D(32, (3, 3), activation='relu', padding='valid')(inputs)
    x = MaxPool2D(pool_size=(2, 2))(x)
    x = Conv2D(64, (3, 3), activation='relu')(x)
    x = MaxPool2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(512, activation='relu')(x)
    x = Dropout(0.5)(x)
    outputs = Dense(_NUM_CLASSES, activation='softmax')(x)

    return tf.python.keras.Model(inputs, outputs)


if __name__ == '__main__':
    training_pipeline()
