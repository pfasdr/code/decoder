import tensorflow

from pfasdr.neural.SeqGAN.discriminator import Discriminator
from pfasdr.neural.SeqGAN.generator import Generator
from pfasdr.neural.SeqGAN.rollout import RollOut
from pfasdr.neural.SeqGAN.sequence_gan_util import pretrain_epoch, \
    set_seeds, sample_fake_data_from_generator, sample_real_data_from_oracle, test_generator
from pfasdr.neural.SeqGAN.target_lstm import TargetLstm
from pfasdr.neural.SeqGAN.config import (ADVERSARIAL_ROUNDS_COUNT, PROBLEM_SEQUENCE_LENGTH, GENERATOR_PRETRAINING_EPOCH_COUNT,
                               DISCRIMINATOR_DROPOUT_KEEPING_PROBABILITY,
                               DISCRIMINATOR_PRETRAINING_EPOCH_COUNT,
                               DISCRIMINATOR_TRAINING_ROUND_COUNT,
                               DISCRIMINATOR_TRAINING_EPOCH_COUNT,
                               DISCRIMINATOR_PRETRAINING_BATCH_COUNT)

from pfasdr.neural.SeqGAN.dataloader import GeneratorDataLoader, DiscriminatorDataloader


def run_sequence_gan():
    set_seeds()

    print('Instantiating data loaders ...')
    generator_data_loader = GeneratorDataLoader()
    target_lstm_data_loader = GeneratorDataLoader()
    discriminator_data_loader = DiscriminatorDataloader()

    print('Instantiating neural networks ...')
    generator = Generator()
    discriminator = Discriminator()
    target_lstm = TargetLstm()

    print('Priming TensorFlow ...')
    config = tensorflow.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.85
    config.gpu_options.allow_growth = False
    session = tensorflow.Session(config=config)
    session.run(tensorflow.global_variables_initializer())

    print('Generating real data using oracle LSTM ...')
    # These are sampled from the oracle data distribution
    sample_real_data_from_oracle(session, target_lstm)
    generator_data_loader.create_batches_from_real_data()

    #  pretrain generator
    print('Pretraining generator ...')
    for pretraining_epoch in range(GENERATOR_PRETRAINING_EPOCH_COUNT):
        training_loss = pretrain_epoch(session, generator, generator_data_loader)

        testing_loss = test_generator(generator, session, target_lstm, target_lstm_data_loader)
        print(f'Generator pretraining --- epoch: {pretraining_epoch}, '
              f'training loss: {training_loss}, testing loss: {testing_loss}')
        del training_loss, testing_loss

    print('Pretraining discriminator ...')
    for pretraining_epoch in range(DISCRIMINATOR_PRETRAINING_EPOCH_COUNT):
        sample_fake_data_from_generator(generator, session)
        discriminator_data_loader.load_train_data()
        for pretraining_batch_index in range(DISCRIMINATOR_PRETRAINING_BATCH_COUNT):
            discriminator_data_loader.reset_pointer()
            for batch_index in range(discriminator_data_loader.batch_count):
                x_batch, y_batch = discriminator_data_loader.next_batch()
                feed = {
                    discriminator.input_x: x_batch,
                    discriminator.input_y: y_batch,
                    discriminator.dropout_keep_prob: DISCRIMINATOR_DROPOUT_KEEPING_PROBABILITY
                }
                session.run(discriminator.train_op, feed)
            print(f'Discriminator pretraining --- epoch: {pretraining_epoch}, '
                    'batch: {pretraining_batch_index}')

    print('Rolling out the generator ...')
    roll_out = RollOut(generator, 0.8)

    print('Start Adversarial Training...')
    for adversarial_round_index in range(ADVERSARIAL_ROUNDS_COUNT):
        # Train the generator
        print('Training generator ...')
        samples = generator.generate(session)
        rewards = roll_out.get_reward(
            session, samples, PROBLEM_SEQUENCE_LENGTH, discriminator)
        feed = {generator.x: samples,
                generator.rewards: rewards}
        _ = session.run(generator.g_updates, feed_dict=feed)

        print('Testing generator ...')
        adversarial_testing_loss = test_generator(generator, session, target_lstm, target_lstm_data_loader)
        print(f'Adversarial round index: {adversarial_round_index}, '
                'test loss: {adversarial_testing_loss}')
        del adversarial_testing_loss

        print('Updating roll-out parameters ...')
        roll_out.update_params()

        print('Training discriminator ...')
        for discriminator_training_round_index in range(DISCRIMINATOR_TRAINING_ROUND_COUNT):
            print(f'Discriminator training round: {discriminator_training_round_index}')

            sample_fake_data_from_generator(generator, session)
            discriminator_data_loader.load_train_data()

            for discriminator_training_epoch in range(DISCRIMINATOR_TRAINING_EPOCH_COUNT):
                print(f'Discriminator training epoch: {discriminator_training_epoch}')

                discriminator_data_loader.reset_pointer()
                for _ in range(discriminator_data_loader.batch_count):
                    x_batch, y_batch = discriminator_data_loader.next_batch()
                    feed = {
                        discriminator.input_x: x_batch,
                        discriminator.input_y: y_batch,
                        discriminator.dropout_keep_prob: DISCRIMINATOR_DROPOUT_KEEPING_PROBABILITY
                    }
                    session.run(discriminator.train_op, feed)


if __name__ == '__main__':
    run_sequence_gan()
