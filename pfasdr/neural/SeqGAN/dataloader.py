import numpy

from pfasdr.neural.SeqGAN.config import PROBLEM_SEQUENCE_LENGTH, BATCH_SIZE
from pfasdr.util.paths import DATA_REAL_FILE_PATH, DATA_EVAL_FILE_PATH, DATA_FAKE_FILE_PATH


def _parse_txt_file_to_list(input_file):
    lines_parsed = []

    with open(input_file, 'r') as data_file_object:
        for line in data_file_object:
            line = line.strip()
            line = line.split()
            line_parsed_to_list = [int(x) for x in line]
            if len(line_parsed_to_list) == PROBLEM_SEQUENCE_LENGTH:
                lines_parsed.append(line_parsed_to_list)

    return lines_parsed


class GeneratorDataLoader():

    def __init__(self):
        self.batch_count = None
        self.pointer = None
        self.sequence_batch = None

    def create_batches_from_real_data(self):
        self._create_batches(data_file=DATA_REAL_FILE_PATH)

    def create_batches_from_evaluation_data(self):
        self._create_batches(data_file=DATA_EVAL_FILE_PATH)

    def _create_batches(self, data_file):
        token_stream = _parse_txt_file_to_list(data_file)

        self.batch_count = int(len(token_stream) / BATCH_SIZE)
        token_stream = token_stream[:self.batch_count * BATCH_SIZE]
        self.sequence_batch = numpy.split(numpy.array(token_stream), self.batch_count, 0)
        self.pointer = 0

    def next_batch(self):
        ret = self.sequence_batch[self.pointer]
        self.pointer = (self.pointer + 1) % self.batch_count
        return ret

    def reset_pointer(self):
        self.pointer = 0


class DiscriminatorDataloader():

    def __init__(self):
        self.sentences = numpy.array([])
        self.labels = numpy.array([])
        self.pointer = None

    def load_train_data(self):
        positives_file = DATA_REAL_FILE_PATH
        negatives_file = DATA_FAKE_FILE_PATH

        # Load data
        positive_examples = _parse_txt_file_to_list(positives_file)
        negative_examples = _parse_txt_file_to_list(negatives_file)
        del positives_file
        del negatives_file
        self.sentences = numpy.array(positive_examples + negative_examples)

        # Generate labels
        positive_labels = [[0, 1] for _ in positive_examples]
        negative_labels = [[1, 0] for _ in negative_examples]
        self.labels = numpy.concatenate([positive_labels, negative_labels], 0)

        # Shuffle the data
        shuffle_indices = numpy.random.permutation(numpy.arange(len(self.labels)))
        self.sentences = self.sentences[shuffle_indices]
        self.labels = self.labels[shuffle_indices]

        # Split batches
        self.batch_count = int(len(self.labels) / BATCH_SIZE)
        self.sentences = self.sentences[:self.batch_count * BATCH_SIZE]
        self.labels = self.labels[:self.batch_count * BATCH_SIZE]
        self.sentences_batches = numpy.split(self.sentences, self.batch_count, 0)
        self.labels_batches = numpy.split(self.labels, self.batch_count, 0)

        self.pointer = 0

    def next_batch(self):
        ret = self.sentences_batches[
            self.pointer], self.labels_batches[self.pointer]
        self.pointer = (self.pointer + 1) % self.batch_count
        return ret

    def reset_pointer(self):
        self.pointer = 0
