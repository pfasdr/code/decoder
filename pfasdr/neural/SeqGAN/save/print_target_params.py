import os
import pickle
from pprint import pprint

from pfasdr.util.paths import SEQGAN_TARGET_PARAMS_FILE_PATH

params = pickle.load(open(os.path.abspath(SEQGAN_TARGET_PARAMS_FILE_PATH), 'rb'), encoding='latin1')

pprint(params)