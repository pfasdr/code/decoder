from pfasdr.neural.w_gan_1d import adversarial_training

from pfasdr.neural.w_gan_1d import get_discriminator_tensors, get_generator_tensors


def adversarial_training_conv_1d():
    adversarial_training(get_generator_tensors=get_generator_tensors,
                         get_discriminator_tensors=get_discriminator_tensors)


if __name__ == '__main__':
    adversarial_training_conv_1d()
