from tensorflow.python.keras.layers.advanced_activations import LeakyReLU
from tensorflow.python.keras.layers.core import Activation, Reshape, Dense
from tensorflow.python.keras.layers.noise import GaussianNoise
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.regularizers import l1_l2

from pfasdr.neural.w_gan_1d import LATENT_LENGTH, PROBLEM_SHAPE, PROBLEM_WIDTH


def get_generator_model_directly():
    hidden_dim = 128
    regulariser = lambda: l1_l2(1e-5, 1e-5)

    model = Sequential(name="generator")

    model.add(Dense(units=int(hidden_dim / 16), name="G_hidden_16th", input_dim=LATENT_LENGTH, kernel_regularizer=regulariser()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 8), name="G_hidden_8th", kernel_regularizer=regulariser()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 4), name="G_hidden_4th", kernel_regularizer=regulariser()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 2), name="G_hidden_half", kernel_regularizer=regulariser()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 1), name="G_hidden_full", kernel_regularizer=regulariser()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=PROBLEM_WIDTH, name="G_flatten", kernel_regularizer=regulariser()))

    model.add(Activation('sigmoid'))

    model.add(Reshape(PROBLEM_SHAPE, name="G_x"))

    return model


def get_discriminator_model_directly():
    hidden_dim = 128
    reg = lambda: l1_l2(1e-5, 1e-5)
    output_activation = "sigmoid"

    model = Sequential(name="D")

    # Reshape input to itself's shape. Stupid but works, so not stupid.
    model.add(Reshape(name="D_reshape", target_shape=(7, ), input_shape=(7,)))

    # model.add(BatchNormalization())

    # Overfitting might be exploited by the generator.
    # So add noise to inputs to avoid overfitting.
    # TODO Does this invalidate the results somewhat?
    model.add(GaussianNoise(0.01))


    model.add(Dense(units=int(hidden_dim / 1), name="D_hidden_full", kernel_regularizer=reg()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 2), name="D_hidden_half", kernel_regularizer=reg()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 4), name="D_hidden_4th", kernel_regularizer=reg()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 8), name="D_hidden_8th", kernel_regularizer=reg()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=int(hidden_dim / 16), name="D_hidden_16th", kernel_regularizer=reg()))
    model.add(LeakyReLU(0.2))

    model.add(Dense(units=1, name="D_y", kernel_regularizer=reg()))

    model.add(Activation(output_activation))

    return model
