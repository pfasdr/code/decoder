import random

import tensorflow
from tensorflow.python.client import device_lib


def tensorflow_version():
    print('tensorflow version:', tensorflow.version.VERSION)


def tensorflow_devices():
    devices = get_gpu_devices()
    print('Devices:', devices)


def get_gpu_devices():
    local_device_protos = device_lib.list_local_devices()
    devices = [x.name for x in local_device_protos if x.device_type == 'GPU']
    return devices


def get_cpu_devices():
    local_device_protos = device_lib.list_local_devices()
    devices = [x.name for x in local_device_protos if x.device_type == 'CPU']
    return devices


def tensorflow_gpu_availability():
    availability = tensorflow.test.is_gpu_available()
    print('Tensorflow can %srun on GPU.' % '' if availability else 'not ')


def tensorflow_use_all_gpus():
    # Create a graph.
    partial_results = []
    for device in get_gpu_devices():
        with tensorflow.device(device):
            print('device:', device)

            # Matrices to multiply
            random.seed('42')
            a = tensorflow.constant([random.random() for _ in range(5)], shape=[2, 3])
            b = tensorflow.constant([random.random() for _ in range(5)], shape=[3, 2])

            # Matrix multiplication
            partial_results.append(tensorflow.matmul(a, b))

    # Combine results
    with tensorflow.device(get_cpu_devices()[0]):
        summation_of_results = tensorflow.add_n(partial_results)

    # Create a session.
    session = tensorflow.Session(
        config=tensorflow.ConfigProto(
            log_device_placement=True))

    # Run the op.
    summation_result = session.run(summation_of_results)

    with tensorflow.Session():
        print(a.eval())
        print('matmul')
        print(b.eval())
        print('* 2')
        print('=')
        print(summation_result)


if __name__ == '__main__':
    tensorflow_version()
    tensorflow_devices()
    tensorflow_gpu_availability()
    tensorflow_use_all_gpus()
