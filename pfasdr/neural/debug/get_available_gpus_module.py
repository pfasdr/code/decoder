import os
from typing import List

from tensorflow.python.client.device_lib import list_local_devices


def list_available_gpus() -> List:
    local_devices: List = list_local_devices()
    available_gpus: List = [
        device.name
        for device in local_devices
        if device.device_type == 'GPU'
    ]

    return available_gpus


def gpu_count():
    available_gpu_count = len(list_available_gpus())

    if os.getenv('HIP_VISIBLE_DEVICES', None) == -1:
        available_gpu_count = 0

    return available_gpu_count
