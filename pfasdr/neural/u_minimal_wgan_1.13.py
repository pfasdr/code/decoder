"""Minimal implementation of Wasserstein GAN for MNIST."""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.python.keras.layers.convolutional import Conv2DTranspose, Conv2D
from tensorflow.python.keras.layers.core import Dense, Flatten, Reshape

from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.python.framework.dtypes import float32
from tensorflow.python.keras.backend import placeholder, ndim

# tf.enable_eager_execution()

config = tf.ConfigProto(device_count={'GPU': 0}, allow_soft_placement=False)
# config = tf.ConfigProto(allow_soft_placement=True)
session = tf.InteractiveSession(
    config=config
)


def leaky_relu(x):
    return tf.maximum(x, 0.2 * x)


def generator(z):
    with tf.variable_scope('generator'):
        z = Dense(units=4096, input_shape=(-1, 28, 28, 1))(z)
        print(z.shape)
        z = Reshape([4, 4, 256])(z)
        print(z.shape)
        z = Conv2DTranspose(filters=128, kernel_size=5, strides=2)(z)
        print(z.shape)
        z = Conv2DTranspose(filters=32, kernel_size=5, strides=2)(z)
        print(z.shape)
        z = Conv2DTranspose(filters=1, kernel_size=4, strides=2,
                            activation=tf.nn.sigmoid)(z)
        # Crop overhang
        print(z.shape)
        print(type(z))
        # z = z[:, 2:-2, 2:-2, :]
        z = tf.slice(z, [0, 2, 2, 0], [1, 28, 28, 1])
        print(type(z))
        print(z.shape)

        return z


def discriminator(x, reuse):
    with tf.variable_scope('discriminator', reuse=reuse):
        x = Conv2D(filters=64, kernel_size=5, strides=2,
                   activation=leaky_relu)(inputs=x)
        x = Conv2D(filters=128, kernel_size=5, strides=2,
                   activation=leaky_relu)(x)
        x = Conv2D(filters=256, kernel_size=4, strides=2,
                   activation=leaky_relu)(x)
        x = Flatten()(x)
        x = Dense(units=1, activation=None)(x)

        return x


with tf.name_scope('placeholders'):
    x_true = placeholder(
        dtype=float32,
        shape=[None, 28, 28, 1],
        ndim=4,
    )
    z = placeholder(
        dtype=float32,
        shape=[None, 128],
        ndim=2,
    )


x_generated = generator(z)

d_true = discriminator(x_true, reuse=False)
d_generated = discriminator(x_generated, reuse=True)

batch_size = 50
with tf.name_scope('regularizer'):
    epsilon = tf.random_uniform([batch_size, 1, 1, 1], 0.0, 1.0)
    x_hat = epsilon * x_true + (1 - epsilon) * x_generated
    d_hat = discriminator(x_hat, reuse=True)

    gradients = tf.gradients(d_hat, x_hat)[0]
    ddx = tf.sqrt(tf.reduce_sum(gradients ** 2, axis=[1, 2]))
    d_regularizer = tf.reduce_mean((ddx - 1.0) ** 2)

with tf.name_scope('loss'):
    g_loss = tf.reduce_mean(d_generated)
    d_loss = (tf.reduce_mean(d_true) - tf.reduce_mean(d_generated) +
              10 * d_regularizer)

with tf.name_scope('optimizer'):
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0, beta2=0.9)

    g_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='generator')
    g_train = optimizer.minimize(g_loss, var_list=g_vars)
    d_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='discriminator')
    d_train = optimizer.minimize(d_loss, var_list=d_vars)

tf.global_variables_initializer().run()

mnist = input_data.read_data_sets('MNIST_data')

for i in range(20000):
    batch = mnist.train.next_batch(batch_size=batch_size)
    images = batch[0].reshape([-1, 28, 28, 1])
    z_train = np.random.randn(batch_size, 128)

    session.run(g_train, feed_dict={z: z_train})
    for j in range(5):
        session.run(d_train, feed_dict={x_true: images, z: z_train})

    if i % 100 == 0:
        print('iter={}/20000'.format(i))
        z_validate = np.random.randn(1, 128)
        generated = x_generated.eval(feed_dict={z: z_validate}).squeeze()

        plt.figure('results')
        plt.imshow(generated, clim=[0, 1], cmap='bone')
        plt.pause(0.001)  # Hack to make the images actually show up
