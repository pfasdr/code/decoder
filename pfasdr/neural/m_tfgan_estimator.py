import numpy as np
import tensorflow as tf
from pfasdr.neural.d_nn_util import load_valid_data_sets_from_csv_files_normalized
from tensorflow.python.keras.layers.core import Dense

tfgan = tf.contrib.gan

x_train, y_train, x_test, y_test = load_valid_data_sets_from_csv_files_normalized()

# See TFGAN's `train_model_on_data.py` for a description of the generator and
# discriminator API.
def generator_fn(generator_inputs):
    generated_data = Dense(units=7)(generator_inputs)
    return generated_data


def discriminator_fn(data, conditioning):
    logits = Dense(units=1)(data)
    return logits


distribution = tf.contrib.distribute.MirroredStrategy(num_gpus=3)
config = tf.estimator.RunConfig(
    model_dir='m_log_dir',
    train_distribute=distribution,
)


# Create GAN estimator.
generator_loss_fn = tfgan.losses.wasserstein_generator_loss
discriminator_loss_fn = tfgan.losses.wasserstein_discriminator_loss
generator_optimizer = tf.train.AdamOptimizer(0.1, 0.5)
discriminator_optimizer = tf.train.AdamOptimizer(0.1, 0.5)
gan_estimator = tfgan.estimator.GANEstimator(
    model_dir='m_log_dir',
    generator_fn=generator_fn,
    discriminator_fn=discriminator_fn,
    generator_loss_fn=generator_loss_fn,
    discriminator_loss_fn=discriminator_loss_fn,
    generator_optimizer=generator_optimizer,
    discriminator_optimizer=discriminator_optimizer,
    config=config,
)


def _tuple_to_dataset(x, y):
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensors((x_train, y_train))
    return dataset


def _train_input_fn():
    dataset = _tuple_to_dataset(x_train, y_train)
    return dataset


steps = 10_000_000

# Train estimator.
gan_estimator.train(
    input_fn=_train_input_fn,
    steps=steps,
)


def _eval_input_fn():
    return x_test, y_test


# Evaluate resulting estimator.
gan_estimator.evaluate(
    input_fn=_eval_input_fn
)


def _predict_input_fn():
    return x_test, y_test


# Generate samples from generator.
predictions = np.array([
    x for x in gan_estimator.predict(predict_input_fn=_predict_input_fn)]
)
