def normalize(prediction):
    """
    Normalize a given prediction for XOR.

    :param prediction: The prediction to be normalized.
    :return: The normalized prediction.
    """
    prediction_normalized = int(prediction)

    return prediction_normalized
