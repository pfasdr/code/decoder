from pfasdr.neural.lib_s_minimal_xor.layer import Layer
from pfasdr.neural.lib_s_minimal_xor.model import Model


def build_model(*,
                hidden_layer_node_count, input_count, output_count):
    model = Model(
        [
            Layer(input_count=input_count,
                  output_count=hidden_layer_node_count),
            Layer(input_count=hidden_layer_node_count,
                  output_count=output_count),
        ]
    )

    return model
