from pfasdr.neural.lib_s_minimal_xor.mean_squared_error import mean_squared_error
from pfasdr.neural.lib_s_minimal_xor.normalize import normalize


def evaluate(*,
             model, evaluation_samples):
    """
    Evaluate a given model on given samples.

    :param model: The model to evaluate
    :param evaluation_samples: The samples to evaluate the model on.
    :return:
    """
    errors = []
    for sample_features, sample_label in evaluation_samples:
        prediction = model.predict(sample_features)

        # Unpack the only entry from the prediction's array for convenience.
        prediction = prediction[0]

        prediction_normalized = normalize(prediction)
        errors.append(sample_label - prediction)

    # Display prediction example
    example_features = evaluation_samples[0][0]
    example_prediction = model.predict(example_features)
    print(example_features, '=NN=>', example_prediction)

    error = mean_squared_error(errors)
    print('Mean squared error over all evaluation samples:', error)

    return error
