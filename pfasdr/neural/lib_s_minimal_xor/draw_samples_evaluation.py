import numpy


def draw_evaluation_samples():
    x_eval = [
        #            _ XOR _
        numpy.array([0, 0]),
        numpy.array([0, 1]),
        numpy.array([1, 0]),
        numpy.array([1, 1]),
    ]
    y_eval = [
        #        = _____
        numpy.array([0]),
        numpy.array([1]),
        numpy.array([1]),
        numpy.array([0]),
    ]

    return x_eval, y_eval
