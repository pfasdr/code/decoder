from pfasdr.neural.lib_s_minimal_xor.evaluate import evaluate


def train_model_on_data(*,
                        epoch_limit, error_metric, learning_rate, log_interval_epochs, model, samples_training,
                        target_error, samples_evaluation):
    for epoch in range(epoch_limit):
        print(f'epoch: {epoch}', end='\r')

        training_errors = []
        for sample_features, sample_label in samples_training:
            # Do a forward pass on the model
            prediction_label = model.predict(sample_features)

            # Compute and record the prediction error
            training_errors.append(float(sample_label) - float(prediction_label))

            # Back-propagate the error
            model.fit(
                error=training_errors[-1],
                learning_rate=learning_rate,
                features=sample_features,
            )

        # Log epoch error to standard output at the log interval
        if epoch % log_interval_epochs == 0:
            # Compute the mean squared error
            training_error = error_metric(training_errors)

            print(f'Epoch {epoch} error: {training_error}')

            print("Evaluating the model on evaluation samples...")
            evaluation_error = evaluate(
                model=model,
                evaluation_samples=samples_evaluation
            )
            print('Evaluation error on evaluation samples was:', evaluation_error)

            print("Evaluating the model on training samples...")
            evaluation_error = evaluate(
                model=model,
                evaluation_samples=samples_training
            )
            print('Evaluation error on training samples was:', evaluation_error)

            if training_error <= target_error:
                print("Target error reached. Stopping Training.")
                training_error = training_errors[-1]
                return training_error, evaluation_error
