from numpy.core.numeric import asarray
from numpy.random.mtrand import uniform

from pfasdr.neural.lib_s_minimal_xor.tanh import tanh, tanh_derivative


class Layer:
    def __init__(self, *,
                 input_count, output_count):
        # Instance variables
        self.delta = None
        self.output = None

        # Init all weights between [-1 .. 1].
        # Each input is connected to all outputs.
        # One line per input and one column per output.
        self.weights = uniform(-1, 1, (input_count, output_count))

    def forward(self, *,
                inputs):
        self.output = tanh(inputs.dot(self.weights))

        return self.output

    def compute_gradient(self, *,
                         error):
        """
        Compute the gradient to by which to update the weights of this layer.

        :param error: The error to minimize with the next update.
        :return: The gradient by which to update this layer's weights by.
        """
        self.delta = error * tanh_derivative(self.output)

        return self.delta.dot(self.weights.T)

    def update_weights(self, *,
                       sample_features, learning_rate):
        """
        Update the weights of this layer according to its delta.

        :param sample_features: The input to update the weights to.
        :param learning_rate: The rate at which to update by.
        :return: None
        """
        # Repackage inputs and delta to make the dimensionality work out
        sample_features = asarray([sample_features])
        delta = asarray([self.delta])

        self.weights += sample_features.T.dot(delta) * learning_rate
