class Model:
    def __init__(self, layers):
        self.layers = layers

    def predict(self, sample_features):
        """
        Predict a label for the given features.

        :param sample_features: Features to predict a label for.
        :return: Predicted label
        """
        # Consider the features the inputs
        inputs = sample_features

        outputs = None
        for layer in self.layers:
            outputs = layer.forward(inputs=inputs)
            # The next layer's inputs is this layers outputs
            inputs = outputs

        # The last layer's outputs is considered the label.
        label = outputs

        return label

    def fit(self, *,
            features, error, learning_rate):
        """
        Fit all layers to the given features.

        :param features: The features to fit this model to.
        :param error: The prediction error this model had before fitment.
        :param learning_rate: Rate at which to learn.
        :return: None
        """
        self._compute_deltas(error=error)
        self._update_weights(sample_features=features, learning_rate=learning_rate)

    def _compute_deltas(self, *,
                        error):
        """
        Compute deltas at each layer, from output to input layer

        :param error: Error at output layer
        :return: None
        """
        for layer in reversed(self.layers):
            error = layer.compute_gradient(error=error)

    def _update_weights(self, *,
                        sample_features, learning_rate):
        """
        Update the weights at each layer, from input to output layer

        :param sample_features:
        :param learning_rate:
        :return:
        """
        for layer in self.layers:
            layer.update_weights(
                sample_features=sample_features,
                learning_rate=learning_rate
            )
            sample_features = layer.output
