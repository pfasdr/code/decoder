import numpy


def draw_samples_training():
    x_train = [
        #            _ XOR _
        numpy.array([0, 0]),
        numpy.array([0, 1]),
        numpy.array([1, 0]),
        numpy.array([1, 1]),
    ]
    y_train = [
        #         = _____
        numpy.array([0]),
        numpy.array([1]),
        numpy.array([1]),
        numpy.array([0]),
    ]

    return x_train, y_train
