# TODO This runs fine
# TODO

"""
An example of distribution approximation using Generative Adversarial Networks
in TensorFlow.

Based on the blog post by Eric Jang:
http://blog.evjang.com/2016/06/generative-adversarial-nets-in.html,

and of course the original GAN paper by Ian Goodfellow et. al.:
https://arxiv.org/abs/1406.2661.

The minibatch discrimination technique is taken from Tim Salimans et. al.:
https://arxiv.org/abs/1606.03498.
"""

import argparse
import numpy as np
import tensorflow as tf
import seaborn

from pfasdr.neural.w_gan_1d import NormalDistribution
from pfasdr.neural.w_gan_1d.gan import GAN
from pfasdr.neural.w_gan_1d.generator_distribution import GeneratorDistribution
from pfasdr.neural.w_gan_1d.train import train

seaborn.set(color_codes=True)

seed = 42
np.random.seed(seed)
tf.set_random_seed(seed)


def main(args):
    model = GAN(args)

    train(
        model=model,
        data=NormalDistribution(),
        gen=GeneratorDistribution(range=8),
        params=args,
    )


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--num-steps', type=int, default=5000,
                        help='the number of training steps to take')
    parser.add_argument('--hidden-size', type=int, default=4,
                        help='MLP hidden size')
    parser.add_argument('--batch-size', type=int, default=8,
                        help='the batch size')
    parser.add_argument('--minibatch', action='store_true',
                        help='use minibatch discrimination')
    parser.add_argument('--log-every', type=int, default=10,
                        help='print loss after this many steps')
    parser.add_argument('--anim-path', type=str, default=None,
                        help='path to the output animation file')
    parser.add_argument('--anim-every', type=int, default=1,
                        help='save every Nth frame for animation')

    args = parser.parse_args()
    print(args)
    return args


if __name__ == '__main__':
    args = parse_args()
    main(args=args)
