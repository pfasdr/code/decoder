from pfasdr.experiments.DynSim.SeqGAN.sequence_gan import run_sequence_gan
from pfasdr.neural.e_valid_to_real import copy_valid_files_to_real_data

copy_valid_files_to_real_data()
run_sequence_gan()
