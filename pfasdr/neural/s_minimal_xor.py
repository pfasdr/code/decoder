from pfasdr.neural.lib_s_minimal_xor.draw_samples_evaluation import draw_evaluation_samples
from pfasdr.neural.lib_s_minimal_xor.draw_samples_training import draw_samples_training
from pfasdr.neural.lib_s_minimal_xor.train import train


def main():
    print("Drawing samples ...")
    x_train, y_train = draw_samples_training()
    x_test, y_test = draw_evaluation_samples()

    error = train(x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test)

    print('Training error:', error)

    return error


if __name__ == '__main__':
    main()
