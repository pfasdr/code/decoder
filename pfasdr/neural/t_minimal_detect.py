import pandas as pd

from pfasdr.neural.d_nn_util import load_valid_data_sets_from_csv_files_normalized, \
    load_invalid_data_sets_from_csv_files_normalized
from pfasdr.neural.lib_s_minimal_xor.train import train


def main():
    print("Drawing samples ...")
    # x_train, y_train, x_test, y_test = load_valid_data_sets_from_csv_files_normalized()

    x_train_valid, y_train_valid, x_test_valid, y_test_valid = load_valid_data_sets_from_csv_files_normalized()
    x_train_invalid, y_train_invalid, x_test_invalid, y_test_invalid = load_invalid_data_sets_from_csv_files_normalized()

    x_train_valid = pd.DataFrame.from_records(x_train_valid)
    x_train_invalid = pd.DataFrame.from_records(x_train_invalid)
    y_train_valid = pd.DataFrame.from_records(y_train_valid)
    y_train_invalid = pd.DataFrame.from_records(y_train_invalid)

    x_test_valid = pd.DataFrame.from_records(x_test_valid)
    x_test_invalid = pd.DataFrame.from_records(x_test_invalid)
    y_test_valid = pd.DataFrame.from_records(y_test_valid)
    y_test_invalid = pd.DataFrame.from_records(y_test_invalid)

    x_train = pd.concat((x_train_valid, x_train_invalid), ignore_index=True)
    y_train = pd.concat((y_train_valid, y_train_invalid), ignore_index=True)
    x_test = pd.concat((x_test_valid, x_test_invalid), ignore_index=True)
    y_test = pd.concat((y_test_valid, y_test_invalid), ignore_index=True)

    x_train = x_train.values
    y_train = y_train.values
    x_test = x_test.values
    y_test = y_test.values

    training_error, evaluation_error = train(x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test)

    print('Training error:', training_error)
    print('Evaluation error:', evaluation_error)

    return training_error, evaluation_error


if __name__ == '__main__':
    main()
