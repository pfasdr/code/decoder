import tensorflow as tf

from pfasdr.neural.w_gan_1d import linear


def generator(input, h_dim):
    h0 = tf.nn.softplus(linear(input, h_dim, 'g0'))
    h1 = linear(h0, 1, 'g1')
    return h1
