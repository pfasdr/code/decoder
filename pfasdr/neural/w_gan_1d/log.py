import tensorflow as tf


def log(x):
    """
    Sometimes discriminiator outputs can reach values close to
    (or even slightly less than) zero due to numerical rounding.
    This just makes sure that we exclude those values so that we don't
    end up with NaNs during optimisation.
    """
    return tf.log(tf.maximum(x, 1e-5))