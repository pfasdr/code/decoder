from itertools import chain, count

from pandas.core.frame import DataFrame
import numpy

from pfasdr.grid_gridcal.sampler.c_plot_samples_lib import plot_by_powers_dataframes
from pfasdr.neural.d_nn_util import load_valid_data_set_from_csv_files, load_invalid_data_set_from_csv_files
from pfasdr.neural.w_gan_1d import LATENT_LENGTH
from pfasdr.neural.k_model import get_generator_model_directly
from pfasdr.util.paths import LAST_GENERATOR_MODEL_CHECKPOINT_FILE_PATH
from pfasdr.util.situations_persistence import COLUMN_NAMES_INPUT


def get_test_powers_from_generator(limit):
    test_powers = DataFrame(columns=COLUMN_NAMES_INPUT)

    generator_model = get_generator_model_directly()
    generator_model.load_weights(filepath=LAST_GENERATOR_MODEL_CHECKPOINT_FILE_PATH)

    for situation_index in count():
        # noise = FIXED_NOISE

        if situation_index == limit:
            break

        # for situation_generated in situations_generated:

        # for _ in range(limit):
        situation_generated = tuple(chain.from_iterable(
            generator_model.predict(
            x=numpy.random.normal(size=(1, LATENT_LENGTH))
        )))  # , steps=1)
        # print(situation_generated.shape)
        # print(situation_generated[0])

        # test_powers[situation_index] = situation_generated

        if not situation_index % 100:
            print(f'Sampling situation \r{situation_index} of {limit}', end='')
            # print(situation_generated - numpy.ones((1, LATENT_LENGTH)))

        # test_powers.loc[situation_index] = situation_generated
        # TODO scaling for plotting only!
        #test_powers.loc[situation_index] = [(1 - power) * 1000 for power in situation_generated]
        test_powers.loc[situation_index] = [power * 10 for power in situation_generated]

    print()

    return test_powers


def _main():
    print('Parsing valid powers ...')
    powers_valid = load_valid_data_set_from_csv_files(use_columns=COLUMN_NAMES_INPUT)
    # powers_valid = d = pandas.DataFrame(numpy.zeros((1, 7)))
    # print(powers_valid.shape)

    sample_count_valid = len(powers_valid)
    print('Valid sample count', sample_count_valid)

    print('Parsing invalid powers ...')
    powers_invalid = load_invalid_data_set_from_csv_files(use_columns=COLUMN_NAMES_INPUT)
    # powers_invalid = pandas.DataFrame(numpy.zeros((1, 7)))
    # print(powers_valid.shape)
    sample_count_invalid = len(powers_invalid)
    print('Invalid sample count', sample_count_invalid)

    print('Parsing test powers ...')
    powers_test = get_test_powers_from_generator(limit=3_000)

    plot_by_powers_dataframes(powers_invalid, powers_test, powers_valid)


if __name__ == '__main__':
    _main()
