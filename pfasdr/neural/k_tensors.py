def get_discriminator_tensors():
    hidden_dim = 2048
    kernel_regularizer = lambda: l1_l2(1e-5, 1e-5)
    output_activation = "sigmoid"

    y = Input(name="discriminator_input", shape=(PROBLEM_WIDTH, ))

    x = Dense(units=int(hidden_dim / 1), name="discriminator_hidden_0", kernel_regularizer=kernel_regularizer())(y)
    x = LeakyReLU(0.2)(x)

    x = Dense(units=int(hidden_dim / 2), name="discriminator_hidden_1", kernel_regularizer=kernel_regularizer())(x)
    x = LeakyReLU(0.2)(x)

    x = Dense(units=int(hidden_dim / 4), name="discriminator_hidden_2", kernel_regularizer=kernel_regularizer())(x)
    x = LeakyReLU(0.2)(x)

    x = Dense(units=1, name="discriminator_y", kernel_regularizer=kernel_regularizer())(x)

    x = Activation(output_activation)(x)

    return y, x


def get_generator_tensors():
    hidden_dim = 2048
    kernel_regularizer = lambda: l1_l2(1e-5, 1e-5)

    y = Input(name="generator_input", shape=(GENERATOR_LATENT_LENGTH,))

    x = Dense(units=int(hidden_dim / 4))(y)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dense(units=int(hidden_dim / 2))(x)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dense(units=int(hidden_dim / 1))(x)
    x = LeakyReLU(alpha=0.2)(x)

    x = Dense(units=PROBLEM_WIDTH, name="generator_x_flat")(x)

    x = Activation('sigmoid')(x)

    # x = Reshape(target_shape=(PROBLEM_WIDTH, ), name="generator_x")(x)

    x = Reshape(target_shape=(PROBLEM_WIDTH, -1))(x)

    return y, x