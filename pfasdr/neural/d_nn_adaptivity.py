import warnings
from collections import deque

import numpy
from tensorflow.python.keras import backend
from tensorflow.python.keras.callbacks import Callback
from scipy import stats


class AdaptLr(Callback):
    """
    Adapt learning rate according to an evaluation metric.
    """

    def __init__(self, monitor='val_loss', adaption_factor=0.9, verbose=0, optimization_mode='min',
                 epsilon=0.1, cooldown_epochs=1):
        super(AdaptLr, self).__init__()

        self._cooldown_epochs = cooldown_epochs
        self._adaption_factor = adaption_factor
        self._epsilon = epsilon
        self._monitor = monitor
        self._verbose = verbose
        self._optimization_mode = optimization_mode

        # Set best yet solution to worst thinkable value
        # So it gets set to the observed result next time
        # monitored_worst = numpy.Inf if self._optimization_mode == 'min' else -numpy.Inf

        self._monitored_history = None
        self._last_adaption = None
        # self._history = None

    def on_train_begin(self, logs=None):
        """
        When training commences, behave as if we just changed the parameters.
        """
        #self._history = deque([backend.get_value(self.model.optimizer.lr)] * 3, 3)
        self._monitored_history = None

    def on_epoch_end(self, epoch, logs=None):
        """
        When an epoch ends, adjust learning rate according to result.
        """
        logs['lr'] = backend.get_value(self.model.optimizer.lr)

        monitored_current = logs.get(self._monitor)
        if monitored_current is None:
            warnings.warn(
                f'Adapt LR conditioned on metric `{self._monitor}` ' +
                f'which is not available. Available metrics are: {",".join(list(logs.keys()))}',
                RuntimeWarning)
            return
        try:
            self._monitored_history.append(monitored_current)
        except AttributeError:
            # This is the first run after initialisation or reset
            # So initialize the history of the monitored value
            self._monitored_history = deque([monitored_current] * 4, maxlen=4)

        # adapt learning rate
        old_lr = float(backend.get_value(self.model.optimizer.lr))
        if self._is_improvement(monitored_current, logs):

            if self._last_adaption == 'reduce':
                # The last adaption was a reduce

                # Increasing would overshoot the optimal learning rate
                # Do not increase the learning rate
                self._last_adaption = None
                new_lr = old_lr
            else:
                # The last adaption was not a reduce

                # Increase learning rate by the given factor
                self._last_adaption = 'increase'
                new_lr = old_lr / self._adaption_factor

            if self._verbose > 0:
                epoch = str(epoch + 1).rjust(5)
                print(f'Epoch {epoch}: ' +
                      f'AdaptLr increasing learning rate ' +
                      f'from {old_lr} to {new_lr}.')
                del epoch
        else:
            if self._last_adaption == 'increase':
                # The last adaption was an increase

                # Decreasing would undershoot the optimal learning rate
                # Do not decrease the learning rate
                self._last_adaption = None
                new_lr = old_lr
            else:
                # The last adaption was not an increase

                # Reduce learning rate by the given factor
                self._last_adaption = 'reduce'
                new_lr = old_lr * self._adaption_factor

            if self._verbose > 0:
                epoch = str(epoch + 1).rjust(5)
                print(f'Epoch {epoch}: ' +
                      f'AdaptLr reducing learning rate ' +
                      f'from {old_lr} to {new_lr}.')
                del epoch

        # logs['adaption_factor'] = numpy.float32(self._adaption_factor)

        # use average of recent history as new learning rate
        # new_lr = numpy.average(list(self._history)[:-1] + [new_lr], weights=[0.1, 0.3, 0.6])
        # self._history.append(new_lr)
        backend.set_value(self.model.optimizer.lr, new_lr)

    def _is_improvement(self, monitored_current, logs):
        monitored_recent_average = numpy.average(self._monitored_history, weights=[.1, .2, .3, .4])

        iqr = stats.iqr(self._monitored_history)
        logs['iqr'] = numpy.float32(iqr)

        if self._optimization_mode == 'min':
            return monitored_current < monitored_recent_average - iqr / 2
        return monitored_current > monitored_recent_average + iqr / 2
