from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import sklearn as sk
from tensorflow import keras
import pfasdr
from pfasdr.neural.d_nn_util import get_invalid_data, get_valid_data


def plot_and_show_history(*, history):
    print('\n--- Learning curve of model training ---\n')
    # summarize history for accuracy and loss
    plt.figure(figsize=(6, 4))
    plt.plot(history.history['acc'], 'g--', label='Accuracy of training data')
    plt.plot(history.history['val_acc'], 'g', label='Accuracy of validation data')
    plt.plot(history.history['loss'], 'r--', label='Loss of training data')
    plt.plot(history.history['val_loss'], 'r', label='Loss of validation data')
    plt.title('Model Accuracy and Loss')
    plt.ylabel('Accuracy and Loss')
    plt.xlabel('Training Epoch')
    plt.ylim(0)
    plt.legend()
    plt.show()


def plot_and_show_confusion_matrix(*, validations, predictions):
    matrix = sk.metrics.confusion_matrix(validations, predictions)
    plt.figure(figsize=(6, 4))
    sns.heatmap(matrix,
                cmap='coolwarm',
                linecolor='white',
                linewidths=1,
                xticklabels=LABELS,
                yticklabels=LABELS,
                annot=True,
                fmt='d')
    plt.title('Confusion Matrix')
    plt.ylabel('True Label')
    plt.xlabel('Predicted Label')
    plt.show()


def show_basic_data_frame_info(data_frame,
                               preview_rows):
    """
    This function shows basic information for the given data frame

    Args:
        data_frame: A Pandas DataFrame expected to contain data
        preview_rows: An integer value of how many rows to preview

    Returns:
        Nothing
    """
    # Shape and how many rows and columns
    print('Number of columns in the data frame: %i' % (data_frame.shape[1]))
    print('Number of rows in the data frame: %i\n' % (data_frame.shape[0]))
    print(f'First {preview_rows} rows of the data frame:\n')
    # Show first few rows
    print(data_frame.head(preview_rows))
    print('\nDescription of data frame:\n')
    # Describe dataset like mean, min, max, etc.
    print(data_frame.describe())


# Set some standard parameters upfront
pd.options.display.float_format = '{:.1f}'.format

# Enable default seaborn look and feel
sns.set()
plt.style.use('ggplot')

LABELS = ['Valid', 'Invalid']

print('\n--- Load, inspect and transform data ---\n')

# x_train_valid, y_train_valid, x_test_valid, y_test_valid = load_valid_data_sets_from_csv_files_data_frame()
# x_train_invalid, y_train_invalid, x_test_invalid, y_test_invalid = load_invalid_data_sets_from_csv_files_data_frame()

# x_train_valid = x_train_valid.shift(periods=x_train_valid.shape[0])
# y_train_valid = y_train_valid.shift(periods=y_train_valid.shape[0])
# x_test_valid = x_test_valid.shift(periods=x_test_valid.shape[0])
# y_test_valid = y_test_valid.shift(periods=y_test_valid.shape[0])

# print(x_train_valid.head(3))
# print(y_train_valid.head(3))

x_train_valid, y_train_valid = get_valid_data()
x_train_invalid, y_train_invalid = get_invalid_data()

# x_train = x_train_invalid
# y_train = y_train_invalid
# x_test = x_test_invalid
# y_test = y_test_invalid

x_train = pd.concat((x_train_valid, x_train_invalid), ignore_index=True)
y_train = pd.concat((y_train_valid, y_train_invalid), ignore_index=True)
# x_test = pd.concat((x_test_valid, x_test_invalid))
# y_test = pd.concat((y_test_valid, y_test_invalid))

# print(x_train[x_train.index.duplicated()])

# Shuffle all data the same way
index_shuffled = np.random.permutation(x_train.shape[0])
x_train = x_train.reindex(index_shuffled)
y_train = y_train.reindex(index_shuffled)
# x_test = x_test.reindex(index_shuffled)
# y_test = y_test.reindex(index_shuffled)

# Describe the data
print('# x train')
show_basic_data_frame_info(x_train, 3)
print('x_train shape: ', x_train.shape)

situation_count = x_train.shape[0]
print(situation_count, 'training grid situations')
assert situation_count >= 100_000

power_count = x_train.shape[1]
assert power_count == 7
print(power_count, 'powers in grid')

print('# y train')
show_basic_data_frame_info(y_train, 3)
print('y_train shape: ', y_train.shape)
print(x_train.shape[0], 'training validity labels')
label_count = y_train.shape[0]
assert label_count == situation_count
class_count = y_train.shape[1]
assert class_count == 1  # 0 --> invalid, 1 --> valid
y_train = keras.utils.np_utils.to_categorical(y_train, 2)
y_train = pd.DataFrame.from_records(y_train)

label_count = y_train.shape[0]
assert label_count == situation_count
class_count = y_train.shape[1]
assert class_count == 2  # one hot: valid, invalid
print('New y_train shape: ', y_train.shape)
show_basic_data_frame_info(y_train, 3)

#print('# x test')
#show_basic_data_frame_info(x_test, 3)
#print('y_test shape:', y_test.shape)

#print('# y test')
#show_basic_data_frame_info(y_test, 3)
#print('y_test shape:', y_test.shape)
#y_test = np_utils.to_categorical(y_test, class_count)
#y_test = pd.DataFrame.from_records(y_test)
#print('New y_test shape:', y_test.shape)
#show_basic_data_frame_info(y_test, 3)

# Convert type for Keras
#x_train = x_train.astype('float32')
#y_train = y_train.astype('float32')
#x_test = x_test.astype('float32')
#y_test = y_test.astype('float32')

# Normalize features for training data set
# x_train['power0'] = feature_normalize(x_train['power0'])
# x_train['power1'] = feature_normalize(x_train['power1'])
# x_train['power2'] = feature_normalize(x_train['power2'])
# x_train['power3'] = feature_normalize(x_train['power3'])
# x_train['power4'] = feature_normalize(x_train['power4'])
# x_train['power5'] = feature_normalize(x_train['power5'])
# x_train['power6'] = feature_normalize(x_train['power6'])

# Normalize features for test data set
# x_test['power0'] = feature_normalize(x_test['power0'])
# x_test['power1'] = feature_normalize(x_test['power1'])
# x_test['power2'] = feature_normalize(x_test['power2'])
# x_test['power3'] = feature_normalize(x_test['power3'])
# x_test['power4'] = feature_normalize(x_test['power4'])
# x_test['power5'] = feature_normalize(x_test['power5'])
# x_test['power6'] = feature_normalize(x_test['power6'])

hidden_class_count = 100

print('\n--- Creating 1D convolutional neural network model ---\n')
model_m = keras.models.Sequential()
model_m.add(keras.layers.Reshape((power_count, ), input_shape=(power_count, )))
model_m.add(keras.layers.Dense(hidden_class_count, activation='relu'))
# model_m.add(Dense(hidden_class_count, activation='relu'))
model_m.add(keras.layers.Dropout(0.5))
model_m.add(keras.layers.Dense(2, activation='softmax'))
print(model_m.summary())

print('\n--- Fit the model ---\n')

# The EarlyStopping callback monitors training accuracy:
# if it fails to improve for two consecutive epochs,
# training stops early
callbacks_list = [
    # keras.callbacks.ModelCheckpoint(
    #     filepath='best_model.{epoch:02d}-{val_loss:.2f}.h5',
    #     monitor='val_loss', save_best_only=True
    # ),
    # keras.callbacks.EarlyStopping(monitor='acc', patience=1)
]

model_m.compile(
    loss='binary_crossentropy',
    metrics=['accuracy'],
    optimizer='rmsprop',
)

# Hyper-parameters
BATCH_SIZE = 1000
EPOCHS = 50

history = model_m.fit(
    x=x_train,
    y=y_train,
    batch_size=BATCH_SIZE,
    epochs=EPOCHS,
    callbacks=callbacks_list,
    # Enable validation to use ModelCheckpoint and EarlyStopping callbacks.
    # validation_split=0.2,
    verbose=True
)

plot_and_show_history(history=history)

print('\n--- Check against test data ---\n')

score = model_m.evaluate(x_test, y_test, verbose=1)

print('\nAccuracy on test data: %0.2f' % score[1])
print('\nLoss on test data: %0.2f' % score[0])

print('\n--- Confusion matrix for test data ---\n')

y_pred_test = model_m.predict(x_test)
# Take the class with the highest probability from the test predictions
max_y_pred_test = np.argmax(y_pred_test, axis=1)
max_y_test = np.argmax(y_test, axis=1)

print(max_y_pred_test)
print(max_y_test)

plot_and_show_confusion_matrix(max_y_test, max_y_pred_test)

print('\n--- Classification report for test data ---\n')

print(sk.metrics.classification_report(max_y_test, max_y_pred_test))
