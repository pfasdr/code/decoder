import numpy as np
import tensorflow as tf
from pfasdr.util.situations_persistence import COLUMN_RANGE_INPUT

from tensorflow.python.training.session_run_hook import SessionRunHook
from tensorflow.python.keras.layers.core import Dense

from pfasdr.grid_gridcal.sampler.c_plot_samples_util import _get_powers_from_situations, _get_validities_from_situations
from pfasdr.neural.d_nn_util import load_valid_data_sets_from_csv_files_normalized

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_integer('noise_dims', 100, 'Number of noise dimensions for generator.')
flags.DEFINE_integer('batch_size', 704488, 'Batch size, which mst divide evenly into the dataset sizes.')
flags.DEFINE_float('learning_rate_discriminator', 0.5, 'Initial learning rate of the discriminator.')
flags.DEFINE_float('learning_rate_generator', 0.5, 'Initial learning rate of the generator.')
flags.DEFINE_integer('max_steps', 100_000_000, 'Number of steps to run trainer.')
flags.DEFINE_string('train_log_dir', 'm_log_dir', 'Directory to log to.')

# Set up the input.
x_train, y_train, x_test, y_test = load_valid_data_sets_from_csv_files_normalized()
real_data = x_train
noise = tf.random_normal([FLAGS.batch_size, FLAGS.noise_dims])


def _unconditional_generator_fn(x):
    print('g in', x)

    x = Dense(units=7)(x)

    print('g out', x)
    return x


def _unconditional_discriminator_fn(x, generator_inputs):
    print('d in', x)

    x = Dense(units=1)(x)

    print('d out', x)
    return x


# Build the generator and discriminator.
# gan_model = tensorflow.contrib.gan.python.
gan_model = tf.contrib.gan.gan_model(
    generator_fn=_unconditional_generator_fn,
    discriminator_fn=_unconditional_discriminator_fn,
    real_data=real_data,
    generator_inputs=noise,
)

# Build the GAN loss.
generator_loss_fn = tf.contrib.gan.losses.wasserstein_generator_loss
discriminator_loss_fn = tf.contrib.gan.losses.wasserstein_discriminator_loss

# Create the train ops, which calculate gradients and apply updates to weights.
generator_optimizer = tf.train.AdamOptimizer(FLAGS.learning_rate_generator, 0.5)
discriminator_optimizer = tf.train.AdamOptimizer(FLAGS.learning_rate_discriminator, 0.5)


class PrintDotSessionRunHook(SessionRunHook):
    def after_create_session(self, session, coord):
        print('After create session.')

    def before_run(self, run_context):
        print('?', end='')

    def after_run(self, run_context, run_values):
        print('!')


def _main_tf_gan():
    gan_loss = tf.contrib.gan.gan_loss(
        gan_model=gan_model,
        generator_loss_fn=generator_loss_fn,
        discriminator_loss_fn=discriminator_loss_fn,
    )

    train_ops = tf.contrib.gan.gan_train_ops(
        gan_model=gan_model,
        gan_loss=gan_loss,
        generator_optimizer=generator_optimizer,
        discriminator_optimizer=discriminator_optimizer,
    )

    # Run the train ops in the alternating training scheme.
    tf.contrib.gan.gan_train(
        train_ops=train_ops,
        hooks=[
            # tf.train.StopAtStepHook(num_steps=FLAGS.max_steps)
        ],
        logdir=FLAGS.train_log_dir,
        save_checkpoint_secs=600,
        save_summaries_steps=100,
        config=None,
    )


def _main_tf_gan_estimator():
    # x = tf.placeholder(tf.float32, [None, 7, 1])
    # z = tf.placeholder(tf.float32, [None, 100, 1])

    print('Creating GAN estimator ...')
    distribution = tf.contrib.distribute.MirroredStrategy(num_gpus=1)
    config = tf.estimator.RunConfig(
        model_dir=FLAGS.train_log_dir,
        train_distribute=distribution,
    )
    gan_estimator = tf.contrib.gan.estimator.GANEstimator(
        model_dir=FLAGS.train_log_dir,
        generator_fn=_unconditional_generator_fn,
        discriminator_fn=_unconditional_discriminator_fn,
        generator_loss_fn=generator_loss_fn,
        discriminator_loss_fn=discriminator_loss_fn,
        generator_optimizer=generator_optimizer,
        discriminator_optimizer=discriminator_optimizer,
        config=config,
    )
    print(gan_estimator)

    print('Training estimator ...')

    def _train_input_fn():
        return x_train, y_train

    def _train_input_fn_dataset():
        #tensor_x = tf.convert_to_tensor(value=x_train, dtype=float32)
        #print(tensor_x)
        #tensor_y = tf.convert_to_tensor(value=y_train, dtype=float32)
        #print(tensor_y)
        #tensors = tensor_x, tensor_y
        #dataset = tf.data.Dataset.zip((
        #    tf.data.Dataset.from_tensors(tensors=x_train),
        #    tf.data.Dataset.from_tensors(tensors=y_train),
        #))
        #print(dataset)

        #iterator = dataset.make_one_shot_iterator()

        # return iterator

        print('Creating dataset ...')

        # dataset = tf.data.Dataset.from_tensor_slices((dict(x_train), y_train))
        # print(dataset)

        validities = _get_validities_from_situations()
        print(type(validities))
        print(validities.shape)
        powers = _get_powers_from_situations()
        print(type(powers))
        print(powers.shape)

        dataset = tf.data.Dataset.from_tensor_slices(
            (dict(powers), validities)
        )

        # Shuffle, repeat, and batch the examples.
        # dataset = dataset.shuffle(1000).repeat().batch(batch_size)

        print(dataset)

        dataset = tf.estimator.inputs.pandas_input_fn(
            x=powers,
            y=validities,
            # batch_size=128,
            # num_epochs=1,
            shuffle=True,
            # queue_capacity=1000,
            # num_threads=1,
            target_column='validity',
        )

        print(type(dataset))

        print(type({key: value for key, value in zip(COLUMN_RANGE_INPUT, x_train)}), type(y_train))
        return tf.data.Dataset.zip(
            (x_train, y_train)
        )

    train_result = gan_estimator.train(
        input_fn=_train_input_fn_dataset,
        steps=None,
        max_steps=10_000_000,
        hooks=[
             # PrintDotSessionRunHook(),
        ],
        saving_listeners=[
        ],
    )
    print(train_result)
    print(train_result.__dict__)

    def _eval_input_fn():
        return x_test, y_test

    print('Evaluating resulting estimator ...')
    result = gan_estimator.evaluate(
        input_fn=_eval_input_fn,
        steps=None,
        hooks=None,
        checkpoint_path=None,
        name=None
    )
    print(result)

    def _predict_input_fn():
        return x_test, y_test

    print('Generating predictions from generator ...')
    predictions = np.array([
        x for x in gan_estimator.predict(
            input_fn=_predict_input_fn,
            predict_keys=None,
            hooks=None,
            checkpoint_path=None,
            yield_single_examples=True,
        )
    ])
    print(predictions)


if __name__ == '__main__':
    # _main_tf_gan()
    _main_tf_gan_estimator()
