import numpy

from pfasdr.neural.d_nn_util import get_valid_data
from pfasdr.neural.w_gan_1d import BATCH_SIZE, PROBLEM_WIDTH, CHANNEL_COUNT


def _situations_training_gen():
    x_train, y_train, = get_valid_data()

    while True:
        for situation_index in range(x_train.shape[0]):
            situation = x_train.iloc[situation_index]
            yield situation


def _situations_testing_gen():
    x_test, y_test, = get_valid_data()

    while True:
        for situation_index in range(x_test.shape[0]):
            situation = x_test.iloc[situation_index]
            yield situation


class BatchGenerator(object):

    _situations = None
    _situations_testing = None

    @classmethod
    def _get_batch_from_generator(cls, generator, field):
        if field is None:
            field = generator()
        batch = numpy.empty((BATCH_SIZE, PROBLEM_WIDTH, CHANNEL_COUNT))
        for situation_index, situation in enumerate(field):
            if situation_index == BATCH_SIZE:
                break
            for power_index, power in enumerate(situation):
                # power /= 20  # normalize
                batch[situation_index][power_index][0] = power
        return batch

    @classmethod
    def get_testing_batch(cls):
        return cls._get_batch_from_generator(
            generator=_situations_testing_gen,
            field=cls._situations_testing)

    @classmethod
    def get_training_batch(cls):
        return cls._get_batch_from_generator(
            generator=_situations_training_gen,
            field=cls._situations)


if __name__ == '__main__':
    batch_training = BatchGenerator.get_training_batch(
        limit=100_000_000,
        file_count=6
    )
    print(batch_training.shape)

    for iteration_index in range(100):
        counter = 0
        for batch in batch_training:
            counter += 1
        print(batch.shape, counter)

    batch_testing = BatchGenerator.get_testing_batch(
        limit=100_000_000,
        file_count=6
    )
    print(batch_testing.shape)

    for iteration_index in range(100):
        counter = 0
        for batch in batch_testing:
            counter += 1
        print(batch.shape, counter)
