import csv
import os

from pfasdr.util.paths import ROOT_PATH

combined_dir_path = \
    ROOT_PATH / 'data' / 'PFASDR.Data.15-node-dynsim-benchmark' / \
    'by sampling' / '2018-03-13' / 'situations'
file_path_valid = combined_dir_path / 'valid.csv'
file_path_invalid = combined_dir_path / 'invalid.csv'

try:
    os.remove(file_path_valid)
except FileNotFoundError:
    print('INFO: No', file_path_valid)
    pass

try:
    os.remove(file_path_invalid)
except FileNotFoundError:
    print('INFO: No', file_path_invalid)
    pass

in_file_names = os.listdir(combined_dir_path)

non_powers = ['Validity', 'Tap0', 'Tap1']
powers = [f'power{power_index}' for power_index in range(6 + 1)]

with open(file_path_valid, 'w') as valid_file:
    with open(file_path_invalid, 'w') as invalid_file:
        writer_valid = csv.DictWriter(
            valid_file,
            fieldnames=[power.lstrip('power') for power in powers]
        )

        writer_invalid = csv.DictWriter(
            invalid_file,
            fieldnames=[power.lstrip('power') for power in powers]
        )

        for file_name in in_file_names:
            input_file_name = str(combined_dir_path / file_name)
            print(input_file_name)
            with open(input_file_name) as input_file:
                reader = csv.DictReader(
                    f=input_file,
                    delimiter=',',
                    fieldnames=None,  # Use first row as fieldnames
                )

                # Choose writer
                if '_valid' in file_name:
                    writer = writer_valid
                elif '_invalid' in file_name:
                    writer = writer_invalid
                else:
                    NotImplementedError()

                for row_index, row in enumerate(reader):
                    if not row_index % 10000:
                        print('\r' + str(row_index), end='')

                    # Filter columns
                    for non_power in non_powers:
                        del row[non_power]

                    # Rename columns
                    for key in powers:
                        row[key.lstrip('power')] = row.pop(key)

                    writer.writerow(row)
                print('\r' + str(row_index))
