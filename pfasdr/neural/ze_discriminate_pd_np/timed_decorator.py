import time


def timed(func):
    def wrapper(*args, **kwargs):
        print(f'Calling {func.__name__} ...')
        start = time.perf_counter()

        value = func(*args, **kwargs)

        end = time.perf_counter()
        print(f'Calling {func.__name__} took {end - start}')

        return value

    return wrapper
