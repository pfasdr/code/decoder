import statistics
import time

import tensorflow as tf

from pfasdr.neural.ze_discriminate_pd_np.get_datasets_module import \
    get_datasets
from pfasdr.neural.ze_discriminate_pd_np.path_templates_module import \
    valid_file_path_template, invalid_file_path_template


def benchmark_dataset(dataset, num_epochs=2):
    tf.print('Iterating data set ...')

    throughput_history = []
    for index_epoch in tf.data.Dataset.range(num_epochs):
        tf.print(f'Iterating for epoch {index_epoch}')
        index = 0

        # The actual benchmark
        tine_start = time.perf_counter()
        for index, _ in enumerate(dataset):
            pass
            # Uncomment for progress reporting
            # if not index % 100:
            #     print('\r' + str(index), end='')
        time_end = time.perf_counter()

        print('\r' + str(index))
        duration = time_end - tine_start
        throughput = index / duration

        tf.print(
            f'Iterating data set took: '
            f'{round(duration, 2)} s in epoch number {index_epoch}. '
            f'This makes for a throughput of {round(throughput)} 1/s'
        )

        if not index_epoch:
            # First round uses cold caches.
            # So do not record it.
            continue
        throughput_history.append(throughput)

    throughput_average = statistics.mean(throughput_history)
    throughput_deviation = statistics.stdev(throughput_history)
    throughput_upper = throughput_average + throughput_deviation
    throughput_lower = throughput_average - throughput_deviation
    tf.print(f'Average dataset entry throughput {round(throughput_average)} '
             f'with a variation of +/- {round(throughput_deviation)}, '
             f'which means {round(throughput_upper)} (+), '
             f'or {round(throughput_lower)} (-).')


def main():
    batch_size = 32
    node_count = 15
    valid_file_path = valid_file_path_template.substitute(
        length=node_count,
    )
    invalid_file_path = invalid_file_path_template.substitute(
        length=node_count,
    )

    dataset_training, dataset_validation, dataset_testing, \
        dataset_size_validation, dataset_size_evaluate, dataset_size_training \
        = get_datasets(
            batch_size=batch_size,
            names=list(range(node_count)),
            invalid_file_path=invalid_file_path,
            valid_file_path=valid_file_path,
        )

    benchmark_dataset(dataset=dataset_training, num_epochs=5)


if __name__ == '__main__':
    main()
