import math
import multiprocessing
import datetime
from pathlib import Path

from numpy import std
from numpy import mean
from tensorflow.python.keras.callbacks import ModelCheckpoint, Callback, \
    EarlyStopping
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.callbacks_v1 import TensorBoard

from pfasdr.neural.ze_discriminate_pd_np.clear_module import clear
from pfasdr.neural.ze_discriminate_pd_np.get_compiled_model_module import \
    get_compiled_model
from pfasdr.neural.ze_discriminate_pd_np.get_datasets_module import \
    get_datasets
from pfasdr.neural.ze_discriminate_pd_np.get_mirrored_strategy_module import \
    get_strategy
from pfasdr.neural.ze_discriminate_pd_np.log_dir_path_module import \
    get_logs_fit_now_dir
from pfasdr.util.python_os.ensure_path_module import ensure_path
from pfasdr.util.paths import ROOT_PATH

clear()


class ClearingCallback(Callback):

    def on_train_batch_begin(self, batch, logs=None):
        # print('Training: batch {} begins at {}'.format(batch, datetime.datetime.now().time()))

        clear()

    def on_train_batch_end(self, batch, logs=None):
        # print('Training: batch {} ends at {}'.format(batch, datetime.datetime.now().time()))
        pass

    def on_test_batch_begin(self, batch, logs=None):
        print('Evaluating: batch {} begins at {}'.format(batch, datetime.datetime.now().time()))

    def on_test_batch_end(self, batch, logs=None):
        print('Evaluating: batch {} ends at {}'.format(batch, datetime.datetime.now().time()))


def learn_grid_constraints(
    *,
    node_count,
    batch_size,
    epochs_training,
    invalid_file_path,
    valid_file_path,
):
    clear()

    dataset_training, dataset_validation, dataset_testing, \
        dataset_size_training, dataset_size_validation, dataset_size_testing \
        = get_datasets(
            batch_size=batch_size,
            names=list(range(node_count)),
            invalid_file_path=invalid_file_path,
            valid_file_path=valid_file_path,
        )

    clear()

    tensorboard_callback = TensorBoard(
        embeddings_freq=0,
        histogram_freq=100,
        log_dir=get_logs_fit_now_dir(node_count=node_count),
        profile_batch=10,
        update_freq='epoch',
        write_graph=True,
    )

    clear()

    early_stopping_callback = EarlyStopping(
        monitor='val_binary_loss', patience=3,
    )

    clear()

    checkpoints_path: \
        Path = ROOT_PATH / 'data' / 'checkpoints' / str(node_count)
    ensure_path(checkpoints_path)
    print('Buffering weights at:', checkpoints_path)
    checkpoint_file_path = str(
        checkpoints_path / f'length_{str(node_count)}.keras'
    )
    val_accuracy = 'val_accuracy'
    model_checkpoint_callback = ModelCheckpoint(
        filepath=checkpoint_file_path,
        save_best_only=True,
        save_weights_only=True,
        save_freq=1,  # in epochs
        monitor='val_binary_accuracy',
        verbose=True,
        mode='max',
    )

    clear()

    clearing_callback = ClearingCallback()

    clear()

    # Train the model, validating it as we go
    with get_strategy().scope():
        model: Sequential = get_compiled_model(input_count=node_count)

        model.build(input_shape=(node_count, node_count))

        model.summary()

    #     try:
    #         model.load_weights(filepath=checkpoint_file_path)
    #         print('Loaded weights')
    #     except OSError as error:
    #         # This is the first run on this grid.
    #         # So the model has not been saved, yet.
    #         print('Could not load weights because of', error)
    #
    #     model.train_on_batch(
    #         x=dataset_training,
    #     )

        steps_per_epoch_training = math.floor(dataset_size_training / batch_size)
        # steps_per_epoch_training += 1  # This seems to avoid stalls
        validation_steps = math.floor(dataset_size_validation / batch_size)
        # validation_steps += 1  # This seems to avoid stalls
        print(
            f'Training model for {epochs_training} epochs and '
            f'{steps_per_epoch_training} steps per epoch and '
            f'validating for {validation_steps} steps.'
        )
        history_callback = model.fit(
           x=dataset_training,
           validation_data=dataset_validation,
           epochs=epochs_training,
           workers=multiprocessing.cpu_count(),
           # workers=multiprocessing.cpu_count() / 4,  # = 8 : 37 ... 47 ms/epoch
           # workers=multiprocessing.cpu_count() / 32,  # = 1 : 37 ... 48  ms/epoch
           use_multiprocessing=True,  # 37 ... 48 ms/epoch
           # use_multiprocessing=False,  # 39 ... 48 ms/epoch, but less variance
           # workers=0,
           shuffle=False,
           callbacks=[
               # tensorboard_callback,
               # early_stopping_callback,
               model_checkpoint_callback,
               # clearing_callback,
           ],
           max_queue_size=10,   # tf.data.experimental.AUTOTUNE,
           steps_per_epoch=steps_per_epoch_training,
           # batch_size=batch_size,
           validation_steps=validation_steps,
        )

    print(list(history_callback.history.keys()))
    if val_accuracy not in history_callback.history.keys():
        raise AttributeError()

    clear()

    # Test the original model
    # steps_testing = 1  # The testing batch is the whole dataset split
    steps_testing = int(dataset_size_testing / batch_size)
    print(f'Testing the original model for {steps_testing} steps')
    test_loss, test_acc = model.evaluate(
        x=dataset_testing,
        # batch_size=dataset_size_training,
        verbose=2,
        sample_weight=None,
        steps=steps_testing,
        callbacks=None,
        max_queue_size=10,
        workers=multiprocessing.cpu_count(),
        use_multiprocessing=True,
    )

    print(f'Original model - test_loss: {test_loss}, test_acc: {test_acc}')

    # Sample model
    steps_sampling_original = 1
    print(f'Sampling original model for {steps_sampling_original} steps')
    y = model.predict(
        x=dataset_testing,
        steps=steps_sampling_original,
    )
    print(y)
    print(f'min: {min(y)}, mean: {mean(y)}, max: {max(y)}, stdev: {std(y)}')

    weights_path = str(
        checkpoints_path /
        datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S" + '.keras')
    )

    # Save model weights to file
    model.save_weights(filepath=weights_path)

    # Restore model from saved weights
    model: Sequential = get_compiled_model(input_count=node_count)
    model.build(input_shape=(node_count, node_count))
    model.load_weights(filepath=weights_path)

    # Evaluate the restored model
    print(f'Testing the original model for {steps_testing} steps ...')
    test_loss, test_acc = model.evaluate(
        x=dataset_testing,
        # batch_size=batch_size_training,
        verbose=2,
        sample_weight=None,
        steps=steps_testing,
        callbacks=None,
        max_queue_size=10,
        workers=multiprocessing.cpu_count(),
        use_multiprocessing=True,
    )
    print(f'Restored model - test_loss: {test_loss}, test_acc: {test_acc}')

    steps_sampling = 1
    print(f'Sampling restored model for {steps_sampling} steps')
    y = model.predict(
        x=dataset_testing,
        steps=steps_sampling,
    )
    print(y)
    print(f'min: {min(y)}, mean: {mean(y)}, max: {max(y)}, stdev: {std(y)}')

    return model
