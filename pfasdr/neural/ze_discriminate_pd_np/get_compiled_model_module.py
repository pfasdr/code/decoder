from typing import List

from tensorflow.python.keras import Sequential
from tensorflow.python.keras.layers import Dense, LeakyReLU, Dropout, \
    BatchNormalization
from tensorflow.python.keras.regularizers import l2


def get_compiled_model(input_count: int) -> Sequential:
    layers: List = [
        Dense(units=input_count, activation='linear'),
        # GaussianNoise(stddev=0.01),
        # BatchNormalization(),
        LeakyReLU(),
        # Dropout(rate=0.5),  # Does not make sense on input.
        # GaussianNoise(stddev=0.1),
        # GlobalAveragePooling1D(),

        Dense(
            units=512, activation='linear',
            kernel_regularizer=l2(l=0.0000001)
        ),
        # BatchNormalization(),
        LeakyReLU(),
        Dropout(rate=0.5),
        # GaussianNoise(stddev=0.1),
        # GlobalAveragePooling1D(),

        Dense(
            units=256, activation='linear',
            kernel_regularizer=l2(l=0.0000001)
        ),
        # BatchNormalization(),
        LeakyReLU(),
        Dropout(rate=0.5),
        # GaussianNoise(stddev=0.1),
        # GlobalAveragePooling1D(),

        Dense(
            units=128, activation='linear',
            kernel_regularizer=l2(l=0.0000001)
        ),
        # BatchNormalization(),
        LeakyReLU(),
        # Dropout(rate=0.5), # Feels wrong on penultimate layer
        # GaussianNoise(stddev=0.1),
        # GlobalAveragePooling1D(),

        BatchNormalization(),  # For sigmoid activation
        Dense(
            units=1, activation='sigmoid',
            kernel_regularizer=l2(l=0.0000001)
        ),
    ]
    model: Sequential = Sequential(layers=layers)

    model.compile(
        optimizer='adam',
        loss='binary_crossentropy',
        metrics=[
            'accuracy',
        ],
    )

    return model
