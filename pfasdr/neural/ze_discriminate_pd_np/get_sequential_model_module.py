import os

from tensorflow.python.keras import Sequential

from pfasdr.neural.ze_discriminate_pd_np.get_compiled_model_module import \
    get_compiled_model
from pfasdr.neural.ze_discriminate_pd_np.get_mirrored_strategy_module import \
    get_strategy


def get_sequential_model(node_count):
    try:
        if os.environ['HIP_VISIBLE_DEVICES'] == '-1':
            model: Sequential = get_compiled_model(input_count=node_count)
            return model
        else:
            with get_strategy().scope():
                model: Sequential = get_compiled_model(input_count=node_count)
                return model
    except KeyError:
        with get_strategy().scope():
            model: Sequential = get_compiled_model(input_count=node_count)
            return model
