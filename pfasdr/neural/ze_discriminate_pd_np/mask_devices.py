import os
from typing import Union, List, Tuple


def mask_devices(
    *,
    visible: Union[
        Tuple[str], Tuple[str, str],
        Tuple[str, str, str], Tuple[str, str, str, str],
        None
    ],
):
    if isinstance(visible, List):
        visible = (str(device_index) for device_index in visible)

    if visible is None:
        os.environ['HIP_VISIBLE_DEVICES'] = '-1'
    if visible is not None:
        os.environ['HIP_VISIBLE_DEVICES'] = ','.join(visible)

    print(
        'HIP_VISIBLE_DEVICES:',
        os.environ['HIP_VISIBLE_DEVICES'].replace(',', ', ')
    )
