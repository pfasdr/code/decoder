import os

import numpy as np
import pandas as pd
import tensorflow as tf
from pandas import DataFrame

from pfasdr.neural.debug.get_available_gpus_module import gpu_count
from pfasdr.neural.ze_discriminate_pd_np.timed_decorator import timed
from pfasdr.neural.ze_discriminate_pd_np.warm_dataset_cache_module import \
    warm_dataset_cache


@timed
def get_datasets(
    *,
    batch_size,
    names,
    invalid_file_path,
    valid_file_path,
    repeat_datasets=True,
):
    # Extract
    data_frame_invalid = pd.read_csv(
        filepath_or_buffer=invalid_file_path,
        names=names,
        dtype='float32',
    )
    data_frame_valid = pd.read_csv(
        filepath_or_buffer=valid_file_path,
        names=names,
        dtype='float32',
    )

    # Transform
    # Limit feature data frame size to smaller dataset
    if data_frame_valid.shape[0] > data_frame_invalid.shape[0]:
        data_frame_valid = data_frame_valid.head(data_frame_invalid.shape[0])
    if data_frame_invalid.shape[0] > data_frame_valid.shape[0]:
        data_frame_invalid = data_frame_invalid.head(data_frame_valid.shape[0])

    # Create label data frames
    target_valid: DataFrame = pd.DataFrame(
        np.ones(data_frame_valid.shape[0], np.float32))
    target_invalid: DataFrame = pd.DataFrame(
        np.zeros(data_frame_invalid.shape[0], np.float32))

    # Construct (features, label) datasets
    dataset_valid: tf.data.Dataset = tf.data.Dataset.from_tensor_slices(
        (data_frame_valid.values, target_valid.values)
    )
    dataset_invalid: tf.data.Dataset = tf.data.Dataset.from_tensor_slices(
        (data_frame_invalid.values, target_invalid.values)
    )

    # TODO
    less_data_factor: float = batch_size / data_frame_invalid.shape[0]
    print('Less data factor:', less_data_factor)
    dataset_size_training: int = int(
        batch_size * 1
        # less_data_factor * 1 * data_frame_valid.shape[0]
    )
    dataset_size_validation: int = int(
        batch_size * 8
        # less_data_factor * 8 * data_frame_valid.shape[0]
    )
    dataset_size_testing: int = int(
        batch_size * 1
        # less_data_factor * 1 * data_frame_valid.shape[0]
    )

    print(
        f'Dataset Sizes - '
        f'Train: {dataset_size_training}, '
        f'Validation: {dataset_size_validation}, '
        f'Test: {dataset_size_testing}'
    )

    # Concatenate dataset
    dataset: tf.data.Dataset = dataset_valid.concatenate(dataset_invalid)

    # Shuffle dataset
    dataset = dataset.shuffle(
        buffer_size=
        data_frame_invalid.shape[0] + data_frame_valid.shape[0],
        seed=1,
        reshuffle_each_iteration=False,
    )

    # sanity_check_dataset(dataset)

    # Does not help measurably, but works at least
    dataset = dataset.cache(filename=os.path.abspath(invalid_file_path))

    # Split dataset into train, test and validation subsets
    dataset_training = dataset.take(dataset_size_training)
    dataset_testing = dataset.skip(dataset_size_training)
    dataset_validation = dataset_testing.skip(dataset_size_validation)
    dataset_testing = dataset_testing.take(dataset_size_testing)

    # Batch datasets
    dataset_training = \
        dataset_training.batch(batch_size=batch_size, drop_remainder=True)
    dataset_validation = \
        dataset_validation.batch(batch_size=batch_size, drop_remainder=True)
    dataset_testing = \
        dataset_testing.batch(batch_size=batch_size, drop_remainder=True)

    # Cache datasets
    dataset_training = dataset_training.cache()
    dataset_validation = dataset_validation.cache()
    # dataset_testing = dataset_testing.cache()  # Only cache fit() data

    # Warm caches
    warm_caches = False
    if warm_caches:
        warm_dataset_cache(dataset_size_training, dataset_training)
        warm_dataset_cache(dataset_size_validation, dataset_validation)

    # Repeat datasets
    if repeat_datasets:
        dataset_training = dataset_training.repeat()
        dataset_validation = dataset_validation.repeat()
        dataset_testing = dataset_testing.repeat()

    # Add prefetch buffers
    # This consumes some memory but avoids some pipeline starvation
    dataset_training = dataset_training.prefetch(
        buffer_size=tf.data.experimental.AUTOTUNE,
        # buffer_size=1,
    )
    dataset_validation = dataset_validation.prefetch(
        buffer_size=tf.data.experimental.AUTOTUNE,
        # buffer_size=1,
    )
    dataset_testing = dataset_testing.prefetch(
        buffer_size=tf.data.experimental.AUTOTUNE,
        # buffer_size=1,
    )

    # This offers a speed up of around 30 % versus no prefetching.
    # This causes high single-thread load.
    devices_to_prefetch_to = \
        (f'/gpu:{gpu_index}' for gpu_index in range(gpu_count()))
    devices_to_prefetch_to = ()
    for device in devices_to_prefetch_to:
        dataset_training = dataset_training.apply(
            transformation_func=tf.data.experimental.prefetch_to_device(
                device=device,
                buffer_size=tf.data.experimental.AUTOTUNE,
            )
        )
        dataset_validation = dataset_validation.apply(
            transformation_func=tf.data.experimental.prefetch_to_device(
                device=device,
                buffer_size=tf.data.experimental.AUTOTUNE,
            )
        )
        # Do not prefetch testing-only data
        # dataset_testing = dataset_testing.apply()

    return \
        dataset_training, dataset_validation, dataset_testing, \
        dataset_size_training, dataset_size_validation, dataset_size_testing
