import os
import datetime

from pfasdr.util.paths import ROOT_PATH

logs_fit_dir: str = str(
    ROOT_PATH / 'data' / 'logs' / 'fit'
)


def get_logs_fit_now_dir(node_count: int) -> str:
    logs_fit_now_dir = str(
        logs_fit_dir +
        os.sep +
        datetime.datetime.now().strftime("%Y%m%d-%H%M%S_" + str(node_count))
    )

    return logs_fit_now_dir
