import tensorflow as tf

from pfasdr.neural.debug.get_available_gpus_module import gpu_count


def get_strategy():
    if gpu_count():
        # Number of synchronisation data packages to be sent via PCIe.
        # Skipping packaging altogether seems to work best.
        # This can be explained by PCIe bandwidth being plenty.
        # So packaging is unnecessary and just takes some time for no reason.
        num_packs = 0

        # cross_device_ops = tf.distribute.HierarchicalCopyAllReduce(
        #     num_packs=num_packs,
        # )

        # cross_device_ops = tf.distribute.ReductionToOneDevice()

        cross_device_ops = tf.distribute.NcclAllReduce(
            num_packs=num_packs,
        )

        strategy = tf.distribute.MirroredStrategy(
            cross_device_ops=cross_device_ops,
        )
    else:
        strategy = tf.distribute.OneDeviceStrategy(
            device="/cpu:0",
        )

    return strategy
