from string import Template

from pfasdr.util.paths import ROOT_PATH

valid_file_path_template = \
    Template(
        str(
            ROOT_PATH.parent / 'PFASDR.Data.Grid.Samples.Linear_1_15' /
            'data' / 'length_$length' /
            'valid.csv'
        )
    )
invalid_file_path_template =  \
    Template(
        str(
            ROOT_PATH.parent / 'PFASDR.Data.Grid.Samples.Linear_1_15' /
            'data' / 'length_$length' /
            'invalid.csv'
        )
    )
