import math


def warm_dataset_cache(dataset_size, dataset):
    print(
        'Warming dataset cache by iterating the whole dataset. '
        'This may take some time ...'
    )

    for index, _ in enumerate(dataset):
        if not index % 100:
            print(f'\r{round(index / dataset_size * 100)} %', end='')
        if index == math.ceil(dataset_size) + 1:
            print()
            break
