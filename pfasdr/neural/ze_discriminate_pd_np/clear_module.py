from tensorflow.python.keras.backend import clear_session

from pfasdr.neural.ze_discriminate_pd_np.timed_decorator import timed


@timed
def clear():
    clear_session()
