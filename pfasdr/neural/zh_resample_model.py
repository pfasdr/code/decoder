from pathlib import Path

from numpy import std
from numpy import mean

from pfasdr.neural.ze_discriminate_pd_np.get_compiled_model_module import \
    get_compiled_model
from pfasdr.neural.ze_discriminate_pd_np.get_datasets_module import get_datasets
from pfasdr.neural.ze_discriminate_pd_np.get_mirrored_strategy_module import \
    get_strategy
from pfasdr.neural.ze_discriminate_pd_np.mask_devices import mask_devices
from pfasdr.util.paths import ROOT_PATH


def main():
    mask_devices(visible=None)

    # Configure
    node_count = 7
    checkpoints_path: Path = \
        ROOT_PATH / 'data' / 'checkpoints' / str(node_count)
    weights_path = str(
        checkpoints_path /
        # '20191107-03:27:20.keras'  # linear grid
        # '20191120-01:48:15.keras'  # jorge's grid gpu
        # '20191120-01:53:53.keras'  # jorge's grid cpu
        # '2019-11-22_03:28:29.keras'  # jorge's grid cpu batch normalization
        '2019-11-22_03:43:17.keras'  # jorge's grid cpu batch normalization
    )

    # Load training data
    batch_size_training = 64
    node_count = 7
    invalid_file_path = \
        ROOT_PATH / 'data' / 'PFASDR.Data.15-node-dynsim-benchmark' / \
        'by sampling' / '2018-03-13' / 'situations' / 'valid.csv'
    valid_file_path = \
        ROOT_PATH / 'data' / 'PFASDR.Data.15-node-dynsim-benchmark' / \
        'by sampling' / '2018-03-13' / 'situations' / 'valid.csv'

    dataset_training, dataset_validation, dataset_testing, \
        batch_size_training, dataset_size_validation, dataset_size_evaluate \
        = get_datasets(
            batch_size=batch_size_training,
            names=list(range(node_count)),
            invalid_file_path=invalid_file_path,
            valid_file_path=valid_file_path,
        )

    # Load Model from saved weights
    with get_strategy().scope():
        model = get_compiled_model(input_count=node_count)
        model.build(input_shape=(node_count, node_count))

        # Sample model
        y = model.predict(
            x=dataset_testing,
            steps=10,
        )
        print(y)
        print(f'min: {min(y)}, mean: {mean(y)}, max: {max(y)}, stdev: {std(y)}')

        model.load_weights(filepath=weights_path)

    # Sample model
    y = model.predict(
        x=dataset_testing,
        steps=10,
    )
    print(y)
    print(f'min: {min(y)}, mean: {mean(y)}, max: {max(y)}, stdev: {std(y)}')

    # Plot samples


if __name__ == '__main__':
    main()
