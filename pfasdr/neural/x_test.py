# TODO This seems to run fine
# TODO Add to CI

import pprint

from tensorflow.python.keras.datasets import mnist
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Dropout
from tensorflow.python.keras.optimizers import RMSprop
from tensorflow.python.keras.utils import to_categorical


class Config(object):
    batch_size = 128
    epochs = 10
    num_classes = 10
    sample_count_training = 60000
    sample_count_testing = 10000


pprint.pprint(Config.__dict__)

# the data, split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = x_train.reshape(Config.sample_count_training, 28 * 28)
x_test = x_test.reshape(Config.sample_count_testing, 28 * 28)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# convert class vectors to binary class matrices
y_train = to_categorical(y_train, Config.num_classes)
y_test = to_categorical(y_test, Config.num_classes)

model = Sequential()
model.add(Dense(512, activation='relu', input_shape=(28 * 28,)))
model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(Config.num_classes, activation='softmax'))
model.summary()

model.compile(
    loss='categorical_crossentropy',
    optimizer=RMSprop(),
    metrics=['accuracy'],
)
history = model.fit(
    x_train,
    y_train,
    batch_size=Config.batch_size,
    epochs=Config.epochs,
    verbose=1,
    validation_data=(x_test, y_test)
)
