# from tensorflow import keras
import gandlf

from pfasdr.neural.w_gan_1d import PlotBatchGenerator
from pfasdr.neural.w_gan_1d import get_generator_tensors, get_discriminator_tensors
from pfasdr.neural.w_gan_1d import get_generator_model, get_discriminator_model

model = gandlf.Model(generator=get_generator_model(get_generator_tensors)[1],
                     discriminator=get_discriminator_model(get_discriminator_tensors))

model.compile(optimizer='adam', loss='binary_crossentropy')

x_training = PlotBatchGenerator.get_batch()

# Latent vector is fed data from a random normal distribution.
# <input_data> represents the real data.
# 'zeros' and 'ones' represent 'fake' and 'real', respectively.
# In this case, the discriminator learns 'real data' -> 1
# and fake 'generated data' -> 0, while the generator learns
# 'generated data' -> 1.
model.fit(['normal', x_training], ['ones', 'zeros'])

# There are many ways to do the same thing, depending on the level
# of specificity you need (especially when training with auxiliary parts).
# The above function could be written as any of the following:
#model.fit(['normal', <input_data>], {'gen_real': 'ones', 'fake': 'zeros'})
#model.fit({'latent_vec': 'normal', 'data_input': <input_data>},
#          {'src': 'ones', 'src_fake': 'zeros'})
#model.fit({'latent_vec': 'normal', 'data_input': <input_data>},
#          {'src_gen': '1', 'src_real': '1', 'src_fake': '0'})

# The model provides a function for predicting the discriminator's
# output on some input data, which is useful for auxiliary classification.
# model_predictions = model.predict([<input_data>])

# The model also provides a function for sampling from the generator.
generated_data = model.sample(['normal'], num_samples=10)

# Under the hood, other functions work like their Keras counterparts.
model.save('/save/path')
model.generator.save('/generator/save/path')
model.discriminator.save('/discriminator/save/path')