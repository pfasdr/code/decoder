import tensorflow as tf
from tf.data import Dataset

from pfasdr.neural.zf_combine_jorge import combined_dir_path
from pfasdr.neural.ze_discriminate_pd_np import get_compiled_model, \
    get_mirrored_strategy

LABEL_COLUMN = 'validity'
LABELS = [0, 1]


def get_dataset(file_path):
    dataset: Dataset = tf.data.experimental.make_csv_dataset(
        file_path,
        batch_size=8,  # Artificially small to make examples easier to show.
        label_name='LABEL_COLUMN',
        num_epochs=1,
        ignore_errors=True,
    )

    return dataset


def main():
    dataset = get_dataset(str(combined_dir_path))

    # show_batch(raw_data)

    strategy = get_mirrored_strategy()
    with strategy.scope():
        model = get_compiled_model(input_count=5)

    model.fit(
        dataset,
    )


if __name__ == '__main__':
    main()


def show_batch(dataset):
    for batch, label in dataset.take(1):
        for key, value in batch.items():
            print("{:20s}: {}".format(key, value.numpy()))
