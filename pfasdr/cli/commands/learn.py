def learn_constraints(
    *,
    grid: str,
    skip: bool,
):
    if skip:
        print('Skipping grid constraint learning.')
        return

    print('Learning grid:', grid)
