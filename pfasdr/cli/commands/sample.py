def sample_grid(
    *,
    grid: str,
    skip: bool,
):
    if skip:
        print('Skipping grid sampling.')
        return

    print('Sampling grid: ', grid)
