from pfasdr.cli.arguments.determine_grids import determine_grids


def test_determine_grids():
    # Test
    grids = determine_grids()

    # Assert
    assert isinstance(grids, tuple)
    for grid in grids:
        assert isinstance(grid, str)
