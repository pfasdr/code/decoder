from pfasdr.cli.commands.test import run_test_suite


def test_test_command():
    # Mock
    skip = True

    # Test
    run_test_suite(
        skip=skip,
    )

    # Assert
    assert True
