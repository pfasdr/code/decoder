from click.testing import CliRunner

from pfasdr.cli.main import main


def test_main_test_skip():
    # Mock
    runner = CliRunner()

    # Test
    result = runner.invoke(main, f'test True'.split())

    # Assert
    assert not result.exception
    assert result.exit_code == 0
    assert result.output.startswith(
        'Testing the code base against the test suite ...'
    )
