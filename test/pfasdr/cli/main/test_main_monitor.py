from click.testing import CliRunner

from pfasdr.cli.main import main


def test_main_monitor():
    # Mock
    runner = CliRunner()

    # Test
    result = runner.invoke(main, 'monitor'.split())

    # Assert
    assert result.exit_code == 0
    assert result.output.startswith('Monitoring training process ...\n')
