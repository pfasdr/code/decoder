from click.testing import CliRunner

from pfasdr.cli.arguments.determine_grids import determine_grids
from pfasdr.cli.main import main


def test_main_learn():
    # Mock
    runner = CliRunner()
    grid = determine_grids()[0]

    # Test
    result = runner.invoke(main, f'learn {grid}'.split())
    print( result.output)

    # Assert
    assert not result.exception
    assert result.exit_code == 0
    assert result.output.startswith('Learning grid: linear_1\n' )
