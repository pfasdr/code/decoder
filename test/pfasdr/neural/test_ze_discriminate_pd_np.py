from multiprocessing.context import Process

from tensorflow.python.keras.models import Sequential

from pfasdr.neural.ze_discriminate_pd_np.learn_grid_constraints_module import \
    learn_grid_constraints
from pfasdr.neural.ze_discriminate_pd_np.mask_devices import mask_devices
from pfasdr.neural.ze_discriminate_pd_np.path_templates_module import \
    valid_file_path_template, invalid_file_path_template


def test_learn_grid_constraints():
    # Setup environment
    mask_devices(visible=None)

    # Mock hyper-parameters
    batch_size = 8
    epochs_training = 3
    node_count = 4
    invalid_file_path = invalid_file_path_template.substitute(
        length=node_count,
    )
    valid_file_path = valid_file_path_template.substitute(
        length=node_count,
    )

    # Test
    def target():
        model = learn_grid_constraints(
            node_count=node_count,
            batch_size=batch_size,
            epochs_training=epochs_training,
            invalid_file_path=invalid_file_path,
            valid_file_path=valid_file_path,
        )

        # Assert
        assert model
        assert isinstance(model, Sequential)

    process = Process(target=target)
    process.start()
    process.join()
