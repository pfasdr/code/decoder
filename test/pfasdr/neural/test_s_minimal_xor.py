import pytest

from pfasdr.neural import s_minimal_xor


@pytest.mark.stage('0')
def test_minimal_xor_convergence():
    training_error, evaluation_error = s_minimal_xor.main()

    assert training_error < 0.1
    assert evaluation_error < 0.1
