import pytest

from pfasdr.neural import t_minimal_detect


@pytest.mark.stage('1')
def _test_minimal_detect_convergence():
    training_error, evaluation_error = t_minimal_detect.main()

    assert training_error < 0.1
    assert evaluation_error < 0.1
